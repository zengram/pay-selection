# NewWebpayRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta_data** | [**\Swagger\Client\Model\WebpayMetaData**](WebpayMetaData.md) |  | [optional] 
**payment_request** | [**\Swagger\Client\Model\WebpayNewPaymentRequest**](WebpayNewPaymentRequest.md) |  | 
**receipt_data** | [**\Swagger\Client\Model\ReceiptData**](ReceiptData.md) |  | [optional] 
**customer_info** | [**\Swagger\Client\Model\WebpayCustomerInfo**](WebpayCustomerInfo.md) |  | [optional] 
**recurring_data** | [**\Swagger\Client\Model\RecurringData**](RecurringData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

