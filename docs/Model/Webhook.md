# Webhook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | **string** | Event | 
**transaction_id** | [**\Swagger\Client\Model\TransactionId**](TransactionId.md) |  | 
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**service_id** | [**\Swagger\Client\Model\ServiceId**](ServiceId.md) |  | 
**date_time** | [**\Swagger\Client\Model\\DateTime**](\DateTime.md) |  | 
**is_test** | [**\Swagger\Client\Model\IsTest**](IsTest.md) |  | 
**email** | [**\Swagger\Client\Model\Email**](Email.md) |  | [optional] 
**phone** | [**\Swagger\Client\Model\Phone**](Phone.md) |  | [optional] 
**description** | [**\Swagger\Client\Model\Description**](Description.md) |  | [optional] 
**custom_fields** | [**\Swagger\Client\Model\CustomFields**](CustomFields.md) |  | [optional] 
**brand** | [**\Swagger\Client\Model\Brand**](Brand.md) |  | [optional] 
**country_code_alpha2** | [**\Swagger\Client\Model\CountryCodeAlpha2**](CountryCodeAlpha2.md) |  | [optional] 
**bank** | [**\Swagger\Client\Model\Bank**](Bank.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

