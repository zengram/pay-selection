# NewPayoutRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**description** | [**\Swagger\Client\Model\Description**](Description.md) |  | 
**customer_info** | [**\Swagger\Client\Model\CustomerInfoPayout**](CustomerInfoPayout.md) |  | [optional] 
**extra_data** | [**\Swagger\Client\Model\ExtraDetailsPayout**](ExtraDetailsPayout.md) |  | [optional] 
**payout_method** | **string** | Payout Method | 
**receipt_data** | [**\Swagger\Client\Model\ReceiptData**](ReceiptData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

