# PaymentTokenPaymentDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Swagger\Client\Model\Type**](Type.md) |  | 
**pay_token** | [**\Swagger\Client\Model\PayToken**](PayToken.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

