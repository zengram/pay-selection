# Ffd105AgentInfoPayingAgent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** | Тег 1044. &lt;br&gt;Наименование операции. | [optional] 
**phones** | [**\Swagger\Client\Model\PhoneNumber[]**](PhoneNumber.md) | Тег 1073. &lt;br&gt;Телефоны платежного агента. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

