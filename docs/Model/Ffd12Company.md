# Ffd12Company

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Тег 1117. &lt;/br&gt; Электронная почта отправителя чека. | [optional] 
**sno** | **string** | Тег 1055. &lt;br&gt; Система налогообложения. Перечисление созначениями:&lt;br /&gt; - «osn» – общая СН;&lt;br /&gt; - «usn_income» – упрощенная СН (доходы);&lt;br /&gt; -«usn_income_outcome» – упрощенная СН (доходы минус расходы);&lt;br /&gt; -«envd» – единый налог на вмененный доход;&lt;br /&gt; -«esn» – единый сельскохозяйственный налог;&lt;br /&gt; -«patent» – патентная СН | 
**inn** | **string** | Тег 1018.  &lt;br&gt; ИНН организации. Используется для предотвращения ошибочных регистраций чеков на ККТ зарегистрированных с другим ИНН (сравнивается со значением в ФН). | 
**payment_address** | **string** | Тег 1187. &lt;/br&gt; Место расчетов. Адрес сайта, зарегистрированный для кассы в налоговом органе (пример заполнения: https://site.ru/) | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

