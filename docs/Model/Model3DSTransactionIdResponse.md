# Model3DSTransactionIdResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_details** | [**\Swagger\Client\Model\Model3DSTransactionIdResponseStateDetails**](Model3DSTransactionIdResponseStateDetails.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

