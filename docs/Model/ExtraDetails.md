# ExtraDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**return_url** | [**\Swagger\Client\Model\ReturnUrl**](ReturnUrl.md) |  | [optional] 
**webhook_url** | [**\Swagger\Client\Model\WebhookUrl**](WebhookUrl.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

