# PaymentCancelRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | [**\Swagger\Client\Model\TransactionId**](TransactionId.md) |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**webhook_url** | [**\Swagger\Client\Model\WebhookUrl**](WebhookUrl.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

