# NewRecurringChangeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurring_id** | [**\Swagger\Client\Model\RecurringId**](RecurringId.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

