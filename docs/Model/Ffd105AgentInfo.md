# Ffd105AgentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Тег 1057. &lt;br&gt; Признак агента (ограничен агентами, введенными в ККТ при фискализации). Возможные значения: &lt;br /&gt; -  «bank_paying_agent» – банковский платежный агент. Оказание услуг покупателю (клиенту) пользователем, являющимся банковским платежным агентом. &lt;br /&gt; -  «bank_paying_subagent» – банковский платежный субагент. Оказание услуг покупателю (клиенту) пользователем, являющимся банковским платежным субагентом. &lt;br /&gt; -  «paying_agent» – платежный агент. Оказание услуг покупателю (клиенту) пользователем, являющимся платежным агентом. &lt;br /&gt; -  «paying_subagent» – платежный субагент. Оказание услуг покупателю (клиенту) пользователем, являющимся платежным субагентом. «attorney» – поверенный. Осуществление расчета с покупателем (клиентом) пользователем, являющимся поверенным. &lt;br /&gt; -  «commission_agent» – комиссионер. Осуществление расчета с покупателем (клиентом) пользователем, являющимся комиссионером. &lt;br /&gt; -  «another» – другой тип агента. Осуществление расчета с покупателем (клиентом) пользователем, являющимся агентом и не являющимся банковским платежным агентом (субагентом), платежным агентом (субагентом), поверенным, комиссионером. | [optional] 
**paying_agent** | [**\Swagger\Client\Model\Ffd105AgentInfoPayingAgent**](Ffd105AgentInfoPayingAgent.md) |  | [optional] 
**receive_payments_operator** | [**\Swagger\Client\Model\Ffd105AgentInfoReceivePaymentsOperator**](Ffd105AgentInfoReceivePaymentsOperator.md) |  | [optional] 
**money_transfer_operator** | [**\Swagger\Client\Model\Ffd105AgentInfoMoneyTransferOperator**](Ffd105AgentInfoMoneyTransferOperator.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

