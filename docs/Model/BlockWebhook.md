# BlockWebhook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_masked** | [**\Swagger\Client\Model\CardMasked**](CardMasked.md) |  | 
**card_holder** | [**\Swagger\Client\Model\CardHolder**](CardHolder.md) |  | [optional] 
**payout_token** | [**\Swagger\Client\Model\PayoutToken**](PayoutToken.md) |  | [optional] 
**rebill_id** | [**\Swagger\Client\Model\RebillId**](RebillId.md) |  | [optional] 
**expiration_date** | [**\Swagger\Client\Model\ExpirationDate**](ExpirationDate.md) |  | [optional] 
**rrn** | [**\Swagger\Client\Model\RRN**](RRN.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

