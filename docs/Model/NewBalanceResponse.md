# NewBalanceResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balance** | [**\Swagger\Client\Model\Balance**](Balance.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

