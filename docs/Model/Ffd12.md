# Ffd12

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\Swagger\Client\Model\Client**](Client.md) |  | 
**company** | [**\Swagger\Client\Model\Ffd12Company**](Ffd12Company.md) |  | 
**items** | [**\Swagger\Client\Model\Ffd12Items[]**](Ffd12Items.md) | Атрибуты позиций. | 
**payments** | [**\Swagger\Client\Model\Payments**](Payments.md) |  | 
**vats** | [**\Swagger\Client\Model\Ffd12Vats[]**](Ffd12Vats.md) | Атрибуты налогов на чек. Необходимо передать либо сумму налога на позицию, либо сумму налога на чек. Если будет переданы и сумма налога на позицию и сумма налога на чек, сервис учтет только сумму налога на чек. | [optional] 
**total** | [**\Swagger\Client\Model\TotalFormat**](TotalFormat.md) |  | 
**additional_check_props** | [**\Swagger\Client\Model\AdditionalCheckProps**](AdditionalCheckProps.md) |  | [optional] 
**cashier** | [**\Swagger\Client\Model\Cashier**](Cashier.md) |  | [optional] 
**additional_user_props** | [**\Swagger\Client\Model\AdditionalUserProps**](AdditionalUserProps.md) |  | [optional] 
**operating_check_props** | [**\Swagger\Client\Model\Ffd12OperatingCheckProps**](Ffd12OperatingCheckProps.md) |  | [optional] 
**sectoral_check_props** | [**\Swagger\Client\Model\Ffd12SectoralItemProps[]**](Ffd12SectoralItemProps.md) | Тег 1261. &lt;br&gt;Включается в состав кассового чека (БСО) в случае, если включение этого отраслевого реквизита кассового чека предусмотрено законодательством Российской Федерации. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

