# AdditionalUserProps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Тег 1085. &lt;br&gt;Наименование дополнительного реквизита пользователя. | 
**value** | **string** | Тег 1086. &lt;br&gt;Значение дополнительного реквизита пользователя. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

