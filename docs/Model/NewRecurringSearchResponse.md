# NewRecurringSearchResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rebill_id** | [**\Swagger\Client\Model\RebillId**](RebillId.md) |  | 
**account_id** | [**\Swagger\Client\Model\AccountId**](AccountId.md) |  | 
**recurring_id** | [**\Swagger\Client\Model\RecurringId**](RecurringId.md) |  | 
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**recurring_status** | [**\Swagger\Client\Model\RecurringStatus**](RecurringStatus.md) |  | 
**recurring_amount** | [**\Swagger\Client\Model\RecurringAmount**](RecurringAmount.md) |  | 
**recurring_paid_count** | [**\Swagger\Client\Model\RecurringPaidCount**](RecurringPaidCount.md) |  | 
**recurring_next_pay** | [**\Swagger\Client\Model\RecurringNextPay**](RecurringNextPay.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

