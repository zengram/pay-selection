# Ffd12SupplierInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phones** | [**\Swagger\Client\Model\PhoneNumber[]**](PhoneNumber.md) | Тег 1171. &lt;br&gt;Телефоны поставщика. | 
**name** | **string** | Тег 1225. &lt;br&gt;Наименование поставщика. | [optional] 
**inn** | **string** | Тег 1226. &lt;br&gt;ИНН поставщика. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

