# PayoutCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payout_details** | [**\Swagger\Client\Model\PayoutCardPayoutDetails**](PayoutCardPayoutDetails.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

