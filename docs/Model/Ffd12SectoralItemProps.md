# Ffd12SectoralItemProps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**federal_id** | [**\Swagger\Client\Model\FederalId**](FederalId.md) |  | 
**date** | [**\Swagger\Client\Model\SectoralDate**](SectoralDate.md) |  | 
**number** | [**\Swagger\Client\Model\SectoralNumber**](SectoralNumber.md) |  | 
**value** | [**\Swagger\Client\Model\SectoralValue**](SectoralValue.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

