# NewRecurringChangeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurring_id** | [**\Swagger\Client\Model\RecurringId**](RecurringId.md) |  | 
**max_periods** | [**\Swagger\Client\Model\MaxPeriods**](MaxPeriods.md) |  | [optional] 
**start_date** | [**\Swagger\Client\Model\StartDateChange**](StartDateChange.md) |  | [optional] 
**interval** | [**\Swagger\Client\Model\IntervalChange**](IntervalChange.md) |  | [optional] 
**period** | [**\Swagger\Client\Model\PeriodChange**](PeriodChange.md) |  | [optional] 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

