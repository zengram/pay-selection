# Ffd105AgentInfoReceivePaymentsOperator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phones** | [**\Swagger\Client\Model\PhoneNumber[]**](PhoneNumber.md) | Тег 1074. &lt;br&gt;Телефоны оператора по приему платежей. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

