# DoubleWebPaymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\Swagger\Client\Model\ErrorCode**](ErrorCode.md) |  | 
**description** | [**\Swagger\Client\Model\ErrorDescription**](ErrorDescription.md) |  | 
**add_details** | [**\Swagger\Client\Model\DoubleWebPaymentResponseAddDetails**](DoubleWebPaymentResponseAddDetails.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

