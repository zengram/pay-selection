# NewPaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**description** | [**\Swagger\Client\Model\Description**](Description.md) |  | 
**rebill_flag** | [**\Swagger\Client\Model\RebillFlag**](RebillFlag.md) |  | [optional] 
**customer_info** | [**\Swagger\Client\Model\CustomerInfo**](CustomerInfo.md) |  | 
**extra_data** | [**\Swagger\Client\Model\ExtraData**](ExtraData.md) |  | [optional] 
**payment_method** | **string** | Payment Method | 
**receipt_data** | [**\Swagger\Client\Model\ReceiptData**](ReceiptData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

