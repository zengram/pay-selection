# WebpayCustomerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | [**\Swagger\Client\Model\Email**](Email.md) |  | [optional] 
**receipt_email** | [**\Swagger\Client\Model\ReceiptEmail**](ReceiptEmail.md) |  | [optional] 
**phone** | [**\Swagger\Client\Model\Phone**](Phone.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\Language**](Language.md) |  | [optional] 
**address** | [**\Swagger\Client\Model\Address**](Address.md) |  | [optional] 
**town** | [**\Swagger\Client\Model\Town**](Town.md) |  | [optional] 
**zip** | [**\Swagger\Client\Model\ZIP**](ZIP.md) |  | [optional] 
**country** | [**\Swagger\Client\Model\Country**](Country.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

