# ConfirmationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | [**\Swagger\Client\Model\TransactionId**](TransactionId.md) |  | 
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**pa_res** | [**\Swagger\Client\Model\PaRes**](PaRes.md) |  | 
**md** | [**\Swagger\Client\Model\MD**](MD.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

