# Ffd105AgentInfoMoneyTransferOperator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phones** | [**\Swagger\Client\Model\PhoneNumber[]**](PhoneNumber.md) | Тег 1075. &lt;br&gt;Телефоны оператора перевода. | [optional] 
**name** | **string** | Тег 1026. &lt;br&gt;Наименование оператора перевода. | [optional] 
**address** | **string** | Тег 1005. &lt;br&gt;Адрес оператора перевода. | [optional] 
**inn** | **string** | Тег 1016. &lt;br&gt;ИНН оператора перевода. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

