# PayoutCardPayoutDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_number** | [**\Swagger\Client\Model\CardNumber**](CardNumber.md) |  | 
**cardholder_name** | [**\Swagger\Client\Model\CardHolder**](CardHolder.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

