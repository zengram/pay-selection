# Ffd12OperatingCheckProps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Тег 1271. &lt;br&gt; Идентификатор операции. &lt;br/&gt; Принимает значения «0» до определения значения реквизита ФНС России. | 
**value** | **string** | Тег 1272. &lt;br&gt; Данные операции. | 
**timestamp** | **string** | Тег 1273. &lt;br&gt; Дата и время операции. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

