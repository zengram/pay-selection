# RedirectTransactionIdResponseStateDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redirect_url** | [**\Swagger\Client\Model\RedirectUrl**](RedirectUrl.md) |  | [optional] 
**redirect_method** | [**\Swagger\Client\Model\RedirectMethod**](RedirectMethod.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

