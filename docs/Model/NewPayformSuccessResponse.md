# NewPayformSuccessResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**form_url** | **string** |  | 
**token** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

