# AddDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**transaction_id** | [**\Swagger\Client\Model\TransactionId**](TransactionId.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

