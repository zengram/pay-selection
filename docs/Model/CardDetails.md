# CardDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_number** | [**\Swagger\Client\Model\CardNumber**](CardNumber.md) |  | 
**exp_month** | [**\Swagger\Client\Model\ExpMonth**](ExpMonth.md) |  | 
**exp_year** | [**\Swagger\Client\Model\ExpYear**](ExpYear.md) |  | 
**cardholder_name** | [**\Swagger\Client\Model\CardHolder**](CardHolder.md) |  | 
**cvc** | [**\Swagger\Client\Model\CVC**](CVC.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

