# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_details** | [**\Swagger\Client\Model\CardDetails**](CardDetails.md) |  | 
**extra_data** | [**\Swagger\Client\Model\ExtraDetails**](ExtraDetails.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

