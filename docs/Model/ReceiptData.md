# ReceiptData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **string** | Дата и время документа внешней системы в формате: «dd.mm.yyyy HH:MM:SS» | 
**external_id** | **string** | Идентификатор документа внешней системы, уникальный среди всех документов, отправляемых в данную группу ККТ. | [optional] 
**receipt** | [**OneOfReceiptDataReceipt**](OneOfReceiptDataReceipt.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

