# NewResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | **int** |  | [optional] 
**amount** | **int** |  | [optional] 
**currency** | **string** |  | [optional] 
**processing_amount** | **int** |  | [optional] 
**processing_currency** | **string** |  | [optional] 
**transaction_status** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

