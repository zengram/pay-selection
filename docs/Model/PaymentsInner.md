# PaymentsInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Swagger\Client\Model\TypeFormat**](TypeFormat.md) |  | 
**sum** | [**\Swagger\Client\Model\PaymentsSumFormat**](PaymentsSumFormat.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

