# RebillResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**processing_amount** | [**\Swagger\Client\Model\ProcessingAmount**](ProcessingAmount.md) |  | 
**processing_currency** | [**\Swagger\Client\Model\ProcessingCurrency**](ProcessingCurrency.md) |  | 
**transaction_status** | [**\Swagger\Client\Model\TransactionStatus**](TransactionStatus.md) |  | 
**rebill_id** | [**\Swagger\Client\Model\RebillId**](RebillId.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

