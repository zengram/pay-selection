# Recurrent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_state** | [**\Swagger\Client\Model\TransactionState**](TransactionState.md) |  | [optional] 
**transaction_id** | [**\Swagger\Client\Model\TransactionId**](TransactionId.md) |  | [optional] 
**transaction_state_details** | [**\Swagger\Client\Model\TransactionStateDetails**](TransactionStateDetails.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

