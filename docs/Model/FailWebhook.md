# FailWebhook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_masked** | [**\Swagger\Client\Model\CardMasked**](CardMasked.md) |  | 
**error_message** | [**\Swagger\Client\Model\ErrorMessage**](ErrorMessage.md) |  | 
**card_holder** | [**\Swagger\Client\Model\CardHolder**](CardHolder.md) |  | [optional] 
**expiration_date** | [**\Swagger\Client\Model\ExpirationDate**](ExpirationDate.md) |  | [optional] 
**rrn** | [**\Swagger\Client\Model\RRN**](RRN.md) |  | [optional] 
**error_code** | [**\Swagger\Client\Model\ErrorCode**](ErrorCode.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

