# RebillRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**description** | [**\Swagger\Client\Model\Description**](Description.md) |  | [optional] 
**rebill_id** | [**\Swagger\Client\Model\RebillId**](RebillId.md) |  | 
**payment_type** | [**\Swagger\Client\Model\PaymentType**](PaymentType.md) |  | [optional] 
**receipt_data** | [**\Swagger\Client\Model\ReceiptData**](ReceiptData.md) |  | [optional] 
**webhook_url** | [**\Swagger\Client\Model\WebhookUrl**](WebhookUrl.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

