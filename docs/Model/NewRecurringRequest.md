# NewRecurringRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rebill_id** | [**\Swagger\Client\Model\RebillId**](RebillId.md) |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**description** | [**\Swagger\Client\Model\Description**](Description.md) |  | [optional] 
**webhook_url** | [**\Swagger\Client\Model\WebhookUrl**](WebhookUrl.md) |  | [optional] 
**account_id** | [**\Swagger\Client\Model\AccountId**](AccountId.md) |  | 
**email** | [**\Swagger\Client\Model\Email**](Email.md) |  | [optional] 
**start_date** | [**\Swagger\Client\Model\StartDate**](StartDate.md) |  | 
**interval** | [**\Swagger\Client\Model\Interval**](Interval.md) |  | 
**period** | [**\Swagger\Client\Model\Period**](Period.md) |  | 
**max_periods** | [**\Swagger\Client\Model\MaxPeriods**](MaxPeriods.md) |  | [optional] 
**receipt_data** | [**\Swagger\Client\Model\ReceiptData**](ReceiptData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

