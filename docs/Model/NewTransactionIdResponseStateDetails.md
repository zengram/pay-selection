# NewTransactionIdResponseStateDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 
**processing_amount** | [**\Swagger\Client\Model\ProcessingAmount**](ProcessingAmount.md) |  | 
**payout_token** | [**\Swagger\Client\Model\PayoutToken**](PayoutToken.md) |  | [optional] 
**processing_currency** | [**\Swagger\Client\Model\ProcessingCurrency**](ProcessingCurrency.md) |  | 
**remaining_amount** | [**\Swagger\Client\Model\RemainingAmount**](RemainingAmount.md) |  | [optional] 
**rebill_id** | [**\Swagger\Client\Model\RebillId**](RebillId.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

