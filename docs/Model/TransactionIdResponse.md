# TransactionIdResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_state** | **string** | Transaction state | 
**transaction_id** | [**\Swagger\Client\Model\TransactionId**](TransactionId.md) |  | 
**order_id** | [**\Swagger\Client\Model\OrderId**](OrderId.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

