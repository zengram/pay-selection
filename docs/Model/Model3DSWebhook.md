# Model3DSWebhook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acs_url** | [**\Swagger\Client\Model\AcsUrl**](AcsUrl.md) |  | 
**pa_req** | [**\Swagger\Client\Model\PaReq**](PaReq.md) |  | 
**md** | [**\Swagger\Client\Model\MD**](MD.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

