# Ffd105Vat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Тег 1199. &lt;br&gt; Устанавливает номер налога в ККТ. Перечисление со значениями: &lt;br /&gt; - «none» – без НДС; &lt;br /&gt; - «vat0» – НДС по ставке 0%; &lt;br /&gt; - «vat10» – НДС чека по ставке 10%; &lt;br /&gt; - «vat110» – НДС чека по расчетной ставке 10/110; &lt;br /&gt; - «vat20» – НДС чека по ставке 20%; &lt;br /&gt; - «vat120» – НДС чека по расчетной ставке 20/120. С 01.04.2019 00:00 при отправке ставки vat18 или vat118 в чеках приход и расход сервис будет возвращать ошибку IncomingValidationException с текстом: \&quot;Передана некорректная ставка налога. С 01.04.2019 ставки НДС 18 и 18/118 не могут использоваться в чеках sell(приход) и buy(расход). | 
**sum** | [**\Swagger\Client\Model\VatSumFormat**](VatSumFormat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

