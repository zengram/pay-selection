# Ffd12MarkQuantity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberator** | **int** | Тег 1230. &lt;br&gt;Числитель дробной части предмета расчета. &lt;br /&gt; Значение реквизита «числитель» (тег 1293) должно быть строго меньше значения реквизита «знаменатель» (тег 1294). | [optional] 
**denominator** | **int** | Тег 1230. &lt;br&gt;Заполняется значением, равным количеству товара в партии (упаковке), имеющей общий код маркировки товара.. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

