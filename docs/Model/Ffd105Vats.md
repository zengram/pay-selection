# Ffd105Vats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Устанавливает номер налога в ККТ. Перечисление со значениями: &lt;br /&gt; - «none» – без НДС. Тег 1105; &lt;br /&gt; - «vat0» – НДС по ставке 0%. Тег 1104; &lt;br /&gt; - «vat10» – НДС чека по ставке 10%. Тег 1103;&lt;br /&gt; - «vat110» – НДС чека по расчетной ставке 10/110. Тег 1107; &lt;br /&gt; - «vat20» – НДС чека по ставке 20%. Тег 1102; &lt;br /&gt; - «vat120» – НДС чека по расчетной ставке 20/120. Тег 1106. &lt;br /&gt;С 01.04.2019 00:00 при отправке ставки vat18 или vat118 в чеках приход и расход сервис будет возвращать ошибку IncomingValidationException с текстом: \&quot;Передана некорректная ставка налога. С 01.04.2019 ставки НДС 18 и 18/118 не могут использоваться в чеках sell(приход) и buy(расход). | 
**sum** | [**\Swagger\Client\Model\VatsSumFormat**](VatsSumFormat.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

