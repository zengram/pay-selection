# WebpayMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_type** | [**\Swagger\Client\Model\WebpayPaymentType**](WebpayPaymentType.md) |  | [optional] 
**type_link** | [**\Swagger\Client\Model\WebpayTypeLink**](WebpayTypeLink.md) |  | [optional] 
**preview_form** | [**\Swagger\Client\Model\WebpayPreviewForm**](WebpayPreviewForm.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

