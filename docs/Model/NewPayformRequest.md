# NewPayformRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success_url** | **string** |  | 
**error_url** | **string** |  | 
**notify_url** | **string** |  | 
**amount** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | 
**currency** | [**\Swagger\Client\Model\Currency**](Currency.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

