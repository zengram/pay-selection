# Swagger\Client\Webhooks_Api

All URIs are relative to *https://gw.payselection.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhooksRecurring**](Webhooks_Api.md#webhooksrecurring) | **POST** /webhooks_recurring | 

# **webhooksRecurring**
> webhooksRecurring($body, $x_site_id, $x_webhook_signature)



Выполняется после того, как регулярная оплата успешно зарегистрирована. Система отправляет запрос на веб-адрес ТСП (который был передан в запросе) с информацией о регулярной оплате.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\Webhooks_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\WebhookRecurring(); // \Swagger\Client\Model\WebhookRecurring | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_webhook_signature = "x_webhook_signature_example"; // string | Webhook signature. [description](#section/Webhook-signature)

try {
    $apiInstance->webhooksRecurring($body, $x_site_id, $x_webhook_signature);
} catch (Exception $e) {
    echo 'Exception when calling Webhooks_Api->webhooksRecurring: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\WebhookRecurring**](../Model/WebhookRecurring.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_webhook_signature** | **string**| Webhook signature. [description](#section/Webhook-signature) |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

