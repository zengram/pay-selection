# Swagger\Client\OperationsApi

All URIs are relative to *https://gw.payselection.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**balance**](OperationsApi.md#balance) | **GET** /balance | 
[**block**](OperationsApi.md#block) | **POST** /payments/requests/block | 
[**cancel**](OperationsApi.md#cancel) | **POST** /payments/cancellation | 
[**charge**](OperationsApi.md#charge) | **POST** /payments/charge | 
[**onfirm**](OperationsApi.md#onfirm) | **POST** /payments/confirmation | 
[**ordersOrderId**](OperationsApi.md#ordersorderid) | **GET** /orders/{OrderId} | 
[**pay**](OperationsApi.md#pay) | **POST** /payments/requests/single | 
[**payout**](OperationsApi.md#payout) | **POST** /payouts | 
[**publicPay**](OperationsApi.md#publicpay) | **POST** /payments/requests/public | 
[**rebill**](OperationsApi.md#rebill) | **POST** /payments/requests/rebill | 
[**recurring**](OperationsApi.md#recurring) | **POST** /payments/recurring | 
[**recurringChange**](OperationsApi.md#recurringchange) | **POST** /payments/recurring/change | 
[**recurringSearch**](OperationsApi.md#recurringsearch) | **POST** /payments/recurring/search | 
[**recurringUnsubscribe**](OperationsApi.md#recurringunsubscribe) | **POST** /payments/recurring/unsubscribe | 
[**refund**](OperationsApi.md#refund) | **POST** /payments/refund | 
[**transactionstransactionId**](OperationsApi.md#transactionstransactionid) | **GET** /transactions/{transactionId} | 
[**unsubscribe**](OperationsApi.md#unsubscribe) | **POST** /payments/unsubscribe | 

# **balance**
> \Swagger\Client\Model\BalanceResponse balance($x_site_id, $x_request_id, $x_request_signature)



Операция проверки доступного баланса для Payout.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->balance($x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->balance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\BalanceResponse**](../Model/BalanceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **block**
> \Swagger\Client\Model\NewPaymentResponse block($body, $x_site_id, $x_request_id, $x_request_signature)



Двухстадийная операция оплаты – денежные средства блокируются на карте.  Если авторизация прошла успешно, необходимо завершить транзакцию в течение 5 дней, если же вы не подтвердите транзакцию запросом на списание в течение 5 дней, снятие денежных средств будет автоматически отменено. Кроме того, есть возможность задать rebillFlag для включения рекуррентных платежей.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPaymentRequest(); // \Swagger\Client\Model\NewPaymentRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->block($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->block: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewPaymentRequest**](../Model/NewPaymentRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancel**
> \Swagger\Client\Model\NewPaymentResponse cancel($body, $x_site_id, $x_request_id, $x_request_signature)



Отмена блокировки средств на карте в рамках ранее проведенной двухстадийной операции оплаты.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\PaymentCancelRequest(); // \Swagger\Client\Model\PaymentCancelRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->cancel($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->cancel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PaymentCancelRequest**](../Model/PaymentCancelRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **charge**
> \Swagger\Client\Model\NewPaymentResponse charge($body, $x_site_id, $x_request_id, $x_request_signature)



Списание средств с карты в рамках проведенной ранее двухстадийной операции оплаты.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\PaymentChargeRequest(); // \Swagger\Client\Model\PaymentChargeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->charge($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->charge: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PaymentChargeRequest**](../Model/PaymentChargeRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **onfirm**
> \Swagger\Client\Model\InlineResponse201 onfirm($body, $x_site_id, $x_request_id, $x_request_signature)



Используется для операций Pay или Block с 3DS после получения результатов аутентификации от банка для завершения одностадийной/двухстадийной операции оплаты.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\ConfirmationRequest(); // \Swagger\Client\Model\ConfirmationRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->onfirm($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->onfirm: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ConfirmationRequest**](../Model/ConfirmationRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\InlineResponse201**](../Model/InlineResponse201.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordersOrderId**
> \Swagger\Client\Model\TransactionIdResponse ordersOrderId($order_id, $x_site_id, $x_request_id, $x_request_signature)



Получить статус по OrderId.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = "order_id_example"; // string | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->ordersOrderId($order_id, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->ordersOrderId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\TransactionIdResponse**](../Model/TransactionIdResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pay**
> \Swagger\Client\Model\NewPaymentResponse pay($body, $x_site_id, $x_request_id, $x_request_signature)



Одностадийная операция оплаты – денежные средства списываются сразу после ее проведения. Кроме того, есть возможность задать rebillFlag для включения рекуррентных платежей.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPaymentRequest(); // \Swagger\Client\Model\NewPaymentRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->pay($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->pay: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewPaymentRequest**](../Model/NewPaymentRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payout**
> \Swagger\Client\Model\NewPaymentResponse payout($body, $x_site_id, $x_request_id, $x_request_signature)



Credit transaction - это тип транзакции, когда денежные средства переводятся на счет держателя карты. Денежные средства зачисляются на карту в течение двух банковских дней.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPayoutRequest(); // \Swagger\Client\Model\NewPayoutRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->payout($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->payout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewPayoutRequest**](../Model/NewPayoutRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **publicPay**
> \Swagger\Client\Model\NewPublicPaymentResponse publicPay($body, $x_site_id, $x_request_id)



Одностадийная операция оплаты – денежные средства списываются сразу после ее проведения. Кроме того, есть возможность задать rebillFlag для включения рекуррентных платежей.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPaymentRequest(); // \Swagger\Client\Model\NewPaymentRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.

try {
    $result = $apiInstance->publicPay($body, $x_site_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->publicPay: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewPaymentRequest**](../Model/NewPaymentRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |

### Return type

[**\Swagger\Client\Model\NewPublicPaymentResponse**](../Model/NewPublicPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rebill**
> \Swagger\Client\Model\NewPaymentResponse rebill($body, $x_site_id, $x_request_id, $x_request_signature)



Операция автоматического списания средств по привязанной ранее карте.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\RebillRequest(); // \Swagger\Client\Model\RebillRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->rebill($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->rebill: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\RebillRequest**](../Model/RebillRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recurring**
> \Swagger\Client\Model\NewRecurringResponse recurring($body, $x_site_id, $x_request_id, $x_request_signature)



Регистрация регулярной оплаты по привязанной ранее карте.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewRecurringRequest(); // \Swagger\Client\Model\NewRecurringRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurring($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurring: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewRecurringRequest**](../Model/NewRecurringRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewRecurringResponse**](../Model/NewRecurringResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recurringChange**
> \Swagger\Client\Model\NewRecurringChangeResponse recurringChange($body, $x_site_id, $x_request_id, $x_request_signature)



Изменение параметров регулярной оплаты. <br> Запрос должен содержать хотя бы один из необязательных параметров.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewRecurringChangeRequest(); // \Swagger\Client\Model\NewRecurringChangeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurringChange($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurringChange: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewRecurringChangeRequest**](../Model/NewRecurringChangeRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewRecurringChangeResponse**](../Model/NewRecurringChangeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recurringSearch**
> \Swagger\Client\Model\NewRecurringSearchResponse recurringSearch($body, $x_site_id, $x_request_id, $x_request_signature)



Поиск регулярной оплаты.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\RecurringSearchBody(); // \Swagger\Client\Model\RecurringSearchBody | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurringSearch($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurringSearch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\RecurringSearchBody**](../Model/RecurringSearchBody.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewRecurringSearchResponse**](../Model/NewRecurringSearchResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recurringUnsubscribe**
> \Swagger\Client\Model\NewRecurringUnsubscribeResponse recurringUnsubscribe($body, $x_site_id, $x_request_id, $x_request_signature)



Отмена регулярной оплаты.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewRecurringUnsubscribeRequest(); // \Swagger\Client\Model\NewRecurringUnsubscribeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurringUnsubscribe($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurringUnsubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewRecurringUnsubscribeRequest**](../Model/NewRecurringUnsubscribeRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewRecurringUnsubscribeResponse**](../Model/NewRecurringUnsubscribeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refund**
> \Swagger\Client\Model\NewPaymentResponse refund($body, $x_site_id, $x_request_id, $x_request_signature)



Только успешная транзакция может быть возвращена

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\RefundRequest(); // \Swagger\Client\Model\RefundRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->refund($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->refund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\RefundRequest**](../Model/RefundRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewPaymentResponse**](../Model/NewPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionstransactionId**
> \Swagger\Client\Model\TransactionIdResponse transactionstransactionId($transaction_id, $x_site_id, $x_request_id, $x_request_signature, $x_request_auth)



Получить статус по TransactionId.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$transaction_id = "transaction_id_example"; // string | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции)
$x_request_auth = "x_request_auth_example"; // string | Тип авторизации

try {
    $result = $apiInstance->transactionstransactionId($transaction_id, $x_site_id, $x_request_id, $x_request_signature, $x_request_auth);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->transactionstransactionId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction_id** | **string**|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции) |
 **x_request_auth** | **string**| Тип авторизации | [optional]

### Return type

[**\Swagger\Client\Model\TransactionIdResponse**](../Model/TransactionIdResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unsubscribe**
> \Swagger\Client\Model\NewUnsubscribeResponse unsubscribe($body, $x_site_id, $x_request_id, $x_request_signature)



Отмена рекуррентных платежей.**При использовании данного метода произойдет отписка по всем зарегистрированным регулярным оплатам в рамках переданного RebillId**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\UnsubscribeRequest(); // \Swagger\Client\Model\UnsubscribeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->unsubscribe($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->unsubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UnsubscribeRequest**](../Model/UnsubscribeRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewUnsubscribeResponse**](../Model/NewUnsubscribeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

