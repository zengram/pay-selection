# Swagger\Client\WebhooksApi

All URIs are relative to *https://gw.payselection.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhooks**](WebhooksApi.md#webhooks) | **POST** /webhooks | 

# **webhooks**
> webhooks($body, $x_site_id, $x_webhook_signature)



Выполняется после того, как оплата успешно проведена – авторизация получена от эмитента. Предназначен для информирования об оплате: система отправляет запрос на веб-адрес ТСП с информацией об оплате, а сервис ТСП должен зафиксировать факт оплаты.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\WebhooksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Webhook(); // \Swagger\Client\Model\Webhook | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_webhook_signature = "x_webhook_signature_example"; // string | Webhook signature. [description](#section/Webhook-signature)

try {
    $apiInstance->webhooks($body, $x_site_id, $x_webhook_signature);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->webhooks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Webhook**](../Model/Webhook.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_webhook_signature** | **string**| Webhook signature. [description](#section/Webhook-signature) |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

