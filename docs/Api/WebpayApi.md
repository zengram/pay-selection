# Swagger\Client\WebpayApi

All URIs are relative to *https://gw.payselection.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](WebpayApi.md#create) | **POST** /webpayments/create | 

# **create**
> \Swagger\Client\Model\NewWebpayResponse create($body, $x_site_id, $x_request_id, $x_request_signature)



Создайте платёж, чтобы Покупатель смог оплатить его

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\WebpayApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewWebpayRequest(); // \Swagger\Client\Model\NewWebpayRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->create($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebpayApi->create: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\NewWebpayRequest**](../Model/NewWebpayRequest.md)|  |
 **x_site_id** | **string**| Идентификатор ТСП |
 **x_request_id** | **string**| Уникальный идентификатор запроса ТСП. |
 **x_request_signature** | **string**| Request signature. [description](#section/Request-signature) |

### Return type

[**\Swagger\Client\Model\NewWebpayResponse**](../Model/NewWebpayResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

