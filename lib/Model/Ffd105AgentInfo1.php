<?php
/**
 * Ffd105AgentInfo1
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PaySelection
 *
 * PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.
 *
 * OpenAPI spec version: v3-oas3
 * Contact: support@payselection.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.42
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * Ffd105AgentInfo1 Class Doc Comment
 *
 * @category Class
 * @description Атрибуты агента.
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Ffd105AgentInfo1 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $swaggerModelName = 'ffd1.05_agent_info_1';

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     */
    protected static $swaggerTypes = [
        'type' => 'string',
        'paying_agent' => '\Swagger\Client\Model\Ffd105AgentInfoPayingAgent',
        'receive_payments_operator' => '\Swagger\Client\Model\Ffd105AgentInfoReceivePaymentsOperator',
        'money_transfer_operator' => '\Swagger\Client\Model\Ffd105AgentInfoMoneyTransferOperator'
    ];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     */
    protected static $swaggerFormats = [
        'type' => null,
        'paying_agent' => null,
        'receive_payments_operator' => null,
        'money_transfer_operator' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'type' => 'type',
        'paying_agent' => 'paying_agent',
        'receive_payments_operator' => 'receive_payments_operator',
        'money_transfer_operator' => 'money_transfer_operator'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'type' => 'setType',
        'paying_agent' => 'setPayingAgent',
        'receive_payments_operator' => 'setReceivePaymentsOperator',
        'money_transfer_operator' => 'setMoneyTransferOperator'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'type' => 'getType',
        'paying_agent' => 'getPayingAgent',
        'receive_payments_operator' => 'getReceivePaymentsOperator',
        'money_transfer_operator' => 'getMoneyTransferOperator'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const TYPE_BANK_PAYING_AGENT = 'bank_paying_agent';
    const TYPE_BANK_PAYING_SUBAGENT = 'bank_paying_subagent';
    const TYPE_PAYING_AGENT = 'paying_agent';
    const TYPE_PAYING_SUBAGENT = 'paying_subagent';
    const TYPE_ATTORNEY = 'attorney';
    const TYPE_COMMISSION_AGENT = 'commission_agent';
    const TYPE_ANOTHER = 'another';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTypeAllowableValues()
    {
        return [
            self::TYPE_BANK_PAYING_AGENT,
            self::TYPE_BANK_PAYING_SUBAGENT,
            self::TYPE_PAYING_AGENT,
            self::TYPE_PAYING_SUBAGENT,
            self::TYPE_ATTORNEY,
            self::TYPE_COMMISSION_AGENT,
            self::TYPE_ANOTHER,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['paying_agent'] = isset($data['paying_agent']) ? $data['paying_agent'] : null;
        $this->container['receive_payments_operator'] = isset($data['receive_payments_operator']) ? $data['receive_payments_operator'] : null;
        $this->container['money_transfer_operator'] = isset($data['money_transfer_operator']) ? $data['money_transfer_operator'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getTypeAllowableValues();
        if (!is_null($this->container['type']) && !in_array($this->container['type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     *
     * @param string $type Тег 1222. <br> Признак агента (ограничен агентами, введенными в ККТ при фискализации). Возможные значения: <br /> -  «bank_paying_agent» – банковский платежный агент. Оказание услуг покупателю (клиенту) пользователем, являющимся банковским платежным агентом. <br /> -  «bank_paying_subagent» – банковский платежный субагент. Оказание услуг покупателю (клиенту) пользователем, являющимся банковским платежным субагентом. <br /> -  «paying_agent» – платежный агент. Оказание услуг покупателю (клиенту) пользователем, являющимся платежным агентом. <br /> -  «paying_subagent» – платежный субагент. Оказание услуг покупателю (клиенту) пользователем, являющимся платежным субагентом. «attorney» – поверенный. Осуществление расчета с покупателем (клиентом) пользователем, являющимся поверенным. <br /> -  «commission_agent» – комиссионер. Осуществление расчета с покупателем (клиентом) пользователем, являющимся комиссионером. <br /> -  «another» – другой тип агента. Осуществление расчета с покупателем (клиентом) пользователем, являющимся агентом и не являющимся банковским платежным агентом (субагентом), платежным агентом (субагентом), поверенным, комиссионером.
     *
     * @return $this
     */
    public function setType($type)
    {
        $allowedValues = $this->getTypeAllowableValues();
        if (!is_null($type) && !in_array($type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets paying_agent
     *
     * @return \Swagger\Client\Model\Ffd105AgentInfoPayingAgent
     */
    public function getPayingAgent()
    {
        return $this->container['paying_agent'];
    }

    /**
     * Sets paying_agent
     *
     * @param \Swagger\Client\Model\Ffd105AgentInfoPayingAgent $paying_agent paying_agent
     *
     * @return $this
     */
    public function setPayingAgent($paying_agent)
    {
        $this->container['paying_agent'] = $paying_agent;

        return $this;
    }

    /**
     * Gets receive_payments_operator
     *
     * @return \Swagger\Client\Model\Ffd105AgentInfoReceivePaymentsOperator
     */
    public function getReceivePaymentsOperator()
    {
        return $this->container['receive_payments_operator'];
    }

    /**
     * Sets receive_payments_operator
     *
     * @param \Swagger\Client\Model\Ffd105AgentInfoReceivePaymentsOperator $receive_payments_operator receive_payments_operator
     *
     * @return $this
     */
    public function setReceivePaymentsOperator($receive_payments_operator)
    {
        $this->container['receive_payments_operator'] = $receive_payments_operator;

        return $this;
    }

    /**
     * Gets money_transfer_operator
     *
     * @return \Swagger\Client\Model\Ffd105AgentInfoMoneyTransferOperator
     */
    public function getMoneyTransferOperator()
    {
        return $this->container['money_transfer_operator'];
    }

    /**
     * Sets money_transfer_operator
     *
     * @param \Swagger\Client\Model\Ffd105AgentInfoMoneyTransferOperator $money_transfer_operator money_transfer_operator
     *
     * @return $this
     */
    public function setMoneyTransferOperator($money_transfer_operator)
    {
        $this->container['money_transfer_operator'] = $money_transfer_operator;

        return $this;
    }

    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed $value Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
