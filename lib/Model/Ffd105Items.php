<?php
/**
 * Ffd105Items
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PaySelection
 *
 * PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.
 *
 * OpenAPI spec version: v3-oas3
 * Contact: support@payselection.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.42
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * Ffd105Items Class Doc Comment
 *
 * @category Class
 * @description Атрибут позиций.
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Ffd105Items implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $swaggerModelName = 'ffd1.05_items';

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     */
    protected static $swaggerTypes = [
        'name' => 'string',
        'price' => '\Swagger\Client\Model\NumberPrice',
        'quantity' => '\Swagger\Client\Model\NumberThreeFormat',
        'sum' => '\Swagger\Client\Model\SumNumberTwoFormat',
        'measurement_unit' => 'string',
        'payment_method' => 'string',
        'payment_object' => 'string',
        'nomenclature_code' => '\Swagger\Client\Model\NomenclatureCode',
        'vat' => '\Swagger\Client\Model\Ffd105Vat',
        'agent_info' => '\Swagger\Client\Model\Ffd105AgentInfo1',
        'supplier_info' => '\Swagger\Client\Model\Ffd105SupplierInfo1',
        'user_data' => 'string',
        'excise' => 'float',
        'country_code' => 'string',
        'declaration_number' => 'string'
    ];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     */
    protected static $swaggerFormats = [
        'name' => null,
        'price' => null,
        'quantity' => null,
        'sum' => null,
        'measurement_unit' => null,
        'payment_method' => null,
        'payment_object' => null,
        'nomenclature_code' => null,
        'vat' => null,
        'agent_info' => null,
        'supplier_info' => null,
        'user_data' => null,
        'excise' => null,
        'country_code' => null,
        'declaration_number' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'name' => 'name',
        'price' => 'price',
        'quantity' => 'quantity',
        'sum' => 'sum',
        'measurement_unit' => 'measurement_unit',
        'payment_method' => 'payment_method',
        'payment_object' => 'payment_object',
        'nomenclature_code' => 'nomenclature_code',
        'vat' => 'vat',
        'agent_info' => 'agent_info',
        'supplier_info' => 'supplier_info',
        'user_data' => 'user_data',
        'excise' => 'excise',
        'country_code' => 'country_code',
        'declaration_number' => 'declaration_number'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'name' => 'setName',
        'price' => 'setPrice',
        'quantity' => 'setQuantity',
        'sum' => 'setSum',
        'measurement_unit' => 'setMeasurementUnit',
        'payment_method' => 'setPaymentMethod',
        'payment_object' => 'setPaymentObject',
        'nomenclature_code' => 'setNomenclatureCode',
        'vat' => 'setVat',
        'agent_info' => 'setAgentInfo',
        'supplier_info' => 'setSupplierInfo',
        'user_data' => 'setUserData',
        'excise' => 'setExcise',
        'country_code' => 'setCountryCode',
        'declaration_number' => 'setDeclarationNumber'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'name' => 'getName',
        'price' => 'getPrice',
        'quantity' => 'getQuantity',
        'sum' => 'getSum',
        'measurement_unit' => 'getMeasurementUnit',
        'payment_method' => 'getPaymentMethod',
        'payment_object' => 'getPaymentObject',
        'nomenclature_code' => 'getNomenclatureCode',
        'vat' => 'getVat',
        'agent_info' => 'getAgentInfo',
        'supplier_info' => 'getSupplierInfo',
        'user_data' => 'getUserData',
        'excise' => 'getExcise',
        'country_code' => 'getCountryCode',
        'declaration_number' => 'getDeclarationNumber'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const PAYMENT_METHOD_FULL_PREPAYMENT = 'full_prepayment';
    const PAYMENT_METHOD_PREPAYMENT = 'prepayment';
    const PAYMENT_METHOD_ADVANCE = 'advance';
    const PAYMENT_METHOD_FULL_PAYMENT = 'full_payment';
    const PAYMENT_METHOD_PARTIAL_PAYMENT = 'partial_payment';
    const PAYMENT_METHOD_CREDIT = 'credit';
    const PAYMENT_METHOD_CREDIT_PAYMENT = 'credit_payment';
    const PAYMENT_OBJECT_COMMODITY = 'commodity';
    const PAYMENT_OBJECT_EXCISE = 'excise';
    const PAYMENT_OBJECT_JOB = 'job';
    const PAYMENT_OBJECT_SERVICE = 'service';
    const PAYMENT_OBJECT_GAMBLING_BET = 'gambling_bet';
    const PAYMENT_OBJECT_GAMBLING_PRIZE = 'gambling_prize';
    const PAYMENT_OBJECT_LOTTERY = 'lottery';
    const PAYMENT_OBJECT_LOTTERY_PRIZE = 'lottery_prize';
    const PAYMENT_OBJECT_INTELLECTUAL_ACTIVITY = 'intellectual_activity';
    const PAYMENT_OBJECT_PAYMENT = 'payment';
    const PAYMENT_OBJECT_AGENT_COMMISSION = 'agent_commission';
    const PAYMENT_OBJECT_COMPOSITE = 'composite';
    const PAYMENT_OBJECT_AWARD = 'award';
    const PAYMENT_OBJECT_ANOTHER = 'another';
    const PAYMENT_OBJECT_PROPERTY_RIGHT = 'property_right';
    const PAYMENT_OBJECT_NON_OPERATING_GAIN = 'non-operating_gain';
    const PAYMENT_OBJECT_INSURANCE_PREMIUM = 'insurance_premium';
    const PAYMENT_OBJECT_SALES_TAX = 'sales_tax';
    const PAYMENT_OBJECT_RESORT_FEE = 'resort_fee';
    const PAYMENT_OBJECT_DEPOSIT = 'deposit';
    const PAYMENT_OBJECT_EXPENSE = 'expense';
    const PAYMENT_OBJECT_PENSION_INSURANCE_IP = 'pension_insurance_ip';
    const PAYMENT_OBJECT_PENSION_INSURANCE = 'pension_insurance';
    const PAYMENT_OBJECT_MEDICAL_INSURANCE_IP = 'medical_insurance_ip';
    const PAYMENT_OBJECT_MEDICAL_INSURANCE = 'medical_insurance';
    const PAYMENT_OBJECT_SOCIAL_INSURANCE = 'social_insurance';
    const PAYMENT_OBJECT_CASINO_PAYMENT = 'casino_payment';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getPaymentMethodAllowableValues()
    {
        return [
            self::PAYMENT_METHOD_FULL_PREPAYMENT,
            self::PAYMENT_METHOD_PREPAYMENT,
            self::PAYMENT_METHOD_ADVANCE,
            self::PAYMENT_METHOD_FULL_PAYMENT,
            self::PAYMENT_METHOD_PARTIAL_PAYMENT,
            self::PAYMENT_METHOD_CREDIT,
            self::PAYMENT_METHOD_CREDIT_PAYMENT,
        ];
    }

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getPaymentObjectAllowableValues()
    {
        return [
            self::PAYMENT_OBJECT_COMMODITY,
            self::PAYMENT_OBJECT_EXCISE,
            self::PAYMENT_OBJECT_JOB,
            self::PAYMENT_OBJECT_SERVICE,
            self::PAYMENT_OBJECT_GAMBLING_BET,
            self::PAYMENT_OBJECT_GAMBLING_PRIZE,
            self::PAYMENT_OBJECT_LOTTERY,
            self::PAYMENT_OBJECT_LOTTERY_PRIZE,
            self::PAYMENT_OBJECT_INTELLECTUAL_ACTIVITY,
            self::PAYMENT_OBJECT_PAYMENT,
            self::PAYMENT_OBJECT_AGENT_COMMISSION,
            self::PAYMENT_OBJECT_COMPOSITE,
            self::PAYMENT_OBJECT_AWARD,
            self::PAYMENT_OBJECT_ANOTHER,
            self::PAYMENT_OBJECT_PROPERTY_RIGHT,
            self::PAYMENT_OBJECT_NON_OPERATING_GAIN,
            self::PAYMENT_OBJECT_INSURANCE_PREMIUM,
            self::PAYMENT_OBJECT_SALES_TAX,
            self::PAYMENT_OBJECT_RESORT_FEE,
            self::PAYMENT_OBJECT_DEPOSIT,
            self::PAYMENT_OBJECT_EXPENSE,
            self::PAYMENT_OBJECT_PENSION_INSURANCE_IP,
            self::PAYMENT_OBJECT_PENSION_INSURANCE,
            self::PAYMENT_OBJECT_MEDICAL_INSURANCE_IP,
            self::PAYMENT_OBJECT_MEDICAL_INSURANCE,
            self::PAYMENT_OBJECT_SOCIAL_INSURANCE,
            self::PAYMENT_OBJECT_CASINO_PAYMENT,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['price'] = isset($data['price']) ? $data['price'] : null;
        $this->container['quantity'] = isset($data['quantity']) ? $data['quantity'] : null;
        $this->container['sum'] = isset($data['sum']) ? $data['sum'] : null;
        $this->container['measurement_unit'] = isset($data['measurement_unit']) ? $data['measurement_unit'] : null;
        $this->container['payment_method'] = isset($data['payment_method']) ? $data['payment_method'] : null;
        $this->container['payment_object'] = isset($data['payment_object']) ? $data['payment_object'] : null;
        $this->container['nomenclature_code'] = isset($data['nomenclature_code']) ? $data['nomenclature_code'] : null;
        $this->container['vat'] = isset($data['vat']) ? $data['vat'] : null;
        $this->container['agent_info'] = isset($data['agent_info']) ? $data['agent_info'] : null;
        $this->container['supplier_info'] = isset($data['supplier_info']) ? $data['supplier_info'] : null;
        $this->container['user_data'] = isset($data['user_data']) ? $data['user_data'] : null;
        $this->container['excise'] = isset($data['excise']) ? $data['excise'] : null;
        $this->container['country_code'] = isset($data['country_code']) ? $data['country_code'] : null;
        $this->container['declaration_number'] = isset($data['declaration_number']) ? $data['declaration_number'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        if ($this->container['price'] === null) {
            $invalidProperties[] = "'price' can't be null";
        }
        if ($this->container['quantity'] === null) {
            $invalidProperties[] = "'quantity' can't be null";
        }
        if ($this->container['sum'] === null) {
            $invalidProperties[] = "'sum' can't be null";
        }
        if ($this->container['payment_method'] === null) {
            $invalidProperties[] = "'payment_method' can't be null";
        }
        $allowedValues = $this->getPaymentMethodAllowableValues();
        if (!is_null($this->container['payment_method']) && !in_array(
                $this->container['payment_method'],
                $allowedValues,
                true
            )) {
            $invalidProperties[] = sprintf(
                "invalid value for 'payment_method', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['payment_object'] === null) {
            $invalidProperties[] = "'payment_object' can't be null";
        }
        $allowedValues = $this->getPaymentObjectAllowableValues();
        if (!is_null($this->container['payment_object']) && !in_array(
                $this->container['payment_object'],
                $allowedValues,
                true
            )) {
            $invalidProperties[] = sprintf(
                "invalid value for 'payment_object', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['vat'] === null) {
            $invalidProperties[] = "'vat' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name Тег 1030. <br>Наименование товара.
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets price
     *
     * @return \Swagger\Client\Model\NumberPrice
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param \Swagger\Client\Model\NumberPrice $price price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets quantity
     *
     * @return \Swagger\Client\Model\NumberThreeFormat
     */
    public function getQuantity()
    {
        return $this->container['quantity'];
    }

    /**
     * Sets quantity
     *
     * @param \Swagger\Client\Model\NumberThreeFormat $quantity quantity
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->container['quantity'] = $quantity;

        return $this;
    }

    /**
     * Gets sum
     *
     * @return \Swagger\Client\Model\SumNumberTwoFormat
     */
    public function getSum()
    {
        return $this->container['sum'];
    }

    /**
     * Sets sum
     *
     * @param \Swagger\Client\Model\SumNumberTwoFormat $sum sum
     *
     * @return $this
     */
    public function setSum($sum)
    {
        $this->container['sum'] = $sum;

        return $this;
    }

    /**
     * Gets measurement_unit
     *
     * @return string
     */
    public function getMeasurementUnit()
    {
        return $this->container['measurement_unit'];
    }

    /**
     * Sets measurement_unit
     *
     * @param string $measurement_unit Тег 1197. <br> Единица измерения товара, работы, услуги, платежа, выплаты, иного предмета расчета. <br> Может принимать одно из значений: <br/> - \"0\" - Применяется для предметов расчета, которые могут быть реализованы поштучно или единицами. <br/> - \"10\" - Грамм. <br/> - \"11\" - Килограмм. <br/> - \"12\" - Тонна. <br/> - \"20\" - Сантиметр. <br/> - \"21\" - Дециметр. <br/>  - \"22\" - Метр. <br/> - \"30\" - Квадратный сантиметр. <br/> - \"31\" - Квадратный дециметр. <br/> - \"32\" - Квадратный метр. <br/> - \"40\" - Миллилитр. <br/> - \"41\" - Литр.  <br/> - \"42\" - Кубический метр. <br/> - \"50\" - Киловатт час. <br/> - \"51\" - Гигакалория. <br/> - \"70\" - Сутки (день). <br/> - \"71\" - Час. <br/> - \"72\" - Минута. <br/> - \"73\" - Секунда. <br/> - \"80\" - Килобайт. <br/> - \"81\" - Мегабайт. <br/> - \"82\" - Гигабайт. <br/> - \"83\" - Терабайт. <br/> - \"255\" - Применяется при использовании иных единиц измерения.
     *
     * @return $this
     */
    public function setMeasurementUnit($measurement_unit)
    {
        $this->container['measurement_unit'] = $measurement_unit;

        return $this;
    }

    /**
     * Gets payment_method
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->container['payment_method'];
    }

    /**
     * Sets payment_method
     *
     * @param string $payment_method Тег 1214. <br> Признак способа расчёта. Возможные значения: <br /> -  «full_prepayment» – предоплата 100%. Полная предварительная оплата до момента передачи предмета расчета. <br /> -  «prepayment» – предоплата. Частичная предварительная оплата до момента передачи предмета расчета. <br /> -  «advance» – аванс. <br /> - «full_payment» – полный расчет. Полная оплата, в том числе с учетом аванса (предварительной оплаты) в момент передачи предмета расчета. <br /> -  «partial_payment» – частичный расчет и кредит. Частичная оплата предмета расчета в момент его передачи с последующей оплатой в кредит <br /> -  «credit» – передача в кредит. Передача предмета расчета без его оплаты в момент его передачи с последующей оплатой в кредит. <br /> -  «credit_payment» – оплата кредита. Оплата предмета расчета после его передачи с оплатой в кредит (оплата кредита).
     *
     * @return $this
     */
    public function setPaymentMethod($payment_method)
    {
        $allowedValues = $this->getPaymentMethodAllowableValues();
        if (!in_array($payment_method, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'payment_method', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['payment_method'] = $payment_method;

        return $this;
    }

    /**
     * Gets payment_object
     *
     * @return string
     */
    public function getPaymentObject()
    {
        return $this->container['payment_object'];
    }

    /**
     * Sets payment_object
     *
     * @param string $payment_object Тег 1212. <br> Признак предмета расчёта: <br /> -   «commodity» – товар. О реализуемом товаре, за исключением подакцизного товара (наименование и иные сведения, описывающие товар).<br /> -   «excise» – подакцизный товар. О реализуемом подакцизном товаре (наименование и иные сведения, описывающие товар).<br /> -   «job» – работа. О выполняемой работе (наименование и иные сведения, описывающие работу).<br /> -   «service» – услуга. Об оказываемой услуге (наименование и иные сведения, описывающие услугу).<br /> -   «gambling_bet» – ставка азартной игры. О приеме ставок при осуществлении деятельности по проведению азартных игр.<br /> -   «gambling_prize» – выигрыш азартной игры. О выплате денежных средств в виде выигрыша при осуществлении деятельности по проведению азартных игр.<br /> -   «lottery» – лотерейный билет. О приеме денежных средств при реализации лотерейных билетов, электронных лотерейных билетов, приеме лотерейных ставок при осуществлении деятельности по проведению лотерей.<br /> -   «lottery_prize» – выигрыш лотереи. О выплате денежных средств в виде выигрыша при осуществлении деятельности по проведению лотерей.<br /> -  «intellectual_activity» – предоставление результатов интеллектуальной деятельности. О предоставлении прав на использование результатов интеллектуальной деятельности или средств индивидуализации<br /> -   «payment» – платеж. Об авансе, задатке, предоплате, кредите, взносе в счет оплаты, пени, штрафе, вознаграждении, бонусе и ином аналогичном предмете расчета. <br /> -   «agent_commission» – агентское вознаграждение. О вознаграждении пользователя, являющегося платежным агентом (субагентом), банковским платежным агентом (субагентом), комиссионером, поверенным или иным агентом.<br /> -   «award» – о взносе в счет оплаты пени, штрафе, вознаграждении, бонусе и ином аналогичном предмете расчета.<br /> -   «another» – иной предмет расчета. О предмете расчета, не относящемуся к выше перечисленным предметам расчета. <br /> -   «property_right» – имущественное право. О передаче имущественных прав. <br /> -   «non-operating_gain» – внереализационный доход. О внереализационном доходе. <br /> -   «insurance_premium» – страховые взносы. О суммах расходов, уменьшающих сумму налога (авансовых платежей) в соответствии с пунктом 3.1 статьи 346.21 Налогового кодекса Российской Федерации. <br /> -   «sales_tax» – торговый сбор. О суммах уплаченного торгового сбора. <br /> - «deposit» – залог. О залоге. <br /> - «expense» – расход. О суммах произведенных расходов в соответствии со статьей 346.16 Налогового кодекса Российской Федерации, уменьшающих доход. «pension_insurance_ip» – взносы на ОПС ИП. О страховых взносах на обязательное пенсионное страхование, уплачиваемых ИП, не производящими выплаты и иные вознаграждения физическим лицам. <br /> - «pension_insurance» – взносы на ОПС. О страховых взносах на обязательное пенсионное страхование, уплачиваемых организациями и ИП, производящими выплаты и иные вознаграждения физическим лицам. <br /> - «medical_insurance_ip» – взносы на ОМС ИП. О страховых взносах на обязательное медицинское страхование, уплачиваемых ИП, не производящими выплаты и иные вознаграждения физическим лицам. <br /> - «medical_insurance» – взносы на ОМС. О страховых взносах на обязательное медицинское страхование, уплачиваемые организациями и ИП, производящими выплаты и иные вознаграждения физическим лицам. <br /> - «social_insurance» – взносы на ОСС. О страховых взносах на обязательное социальное страхование на случай временной нетрудоспособности и в связи с материнством, на обязательное социальное страхование от несчастных случаев на производстве и профессиональных заболеваний. <br /> - «casino_payment» – платеж казино. О приеме и выплате денежных средств при осуществлении деятельности казино с использованием обменных знаков казино, в зале игровых автоматов.
     *
     * @return $this
     */
    public function setPaymentObject($payment_object)
    {
        $allowedValues = $this->getPaymentObjectAllowableValues();
        if (!in_array($payment_object, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'payment_object', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['payment_object'] = $payment_object;

        return $this;
    }

    /**
     * Gets nomenclature_code
     *
     * @return \Swagger\Client\Model\NomenclatureCode
     */
    public function getNomenclatureCode()
    {
        return $this->container['nomenclature_code'];
    }

    /**
     * Sets nomenclature_code
     *
     * @param \Swagger\Client\Model\NomenclatureCode $nomenclature_code nomenclature_code
     *
     * @return $this
     */
    public function setNomenclatureCode($nomenclature_code)
    {
        $this->container['nomenclature_code'] = $nomenclature_code;

        return $this;
    }

    /**
     * Gets vat
     *
     * @return \Swagger\Client\Model\Ffd105Vat
     */
    public function getVat()
    {
        return $this->container['vat'];
    }

    /**
     * Sets vat
     *
     * @param \Swagger\Client\Model\Ffd105Vat $vat vat
     *
     * @return $this
     */
    public function setVat($vat)
    {
        $this->container['vat'] = $vat;

        return $this;
    }

    /**
     * Gets agent_info
     *
     * @return \Swagger\Client\Model\Ffd105AgentInfo1
     */
    public function getAgentInfo()
    {
        return $this->container['agent_info'];
    }

    /**
     * Sets agent_info
     *
     * @param \Swagger\Client\Model\Ffd105AgentInfo1 $agent_info agent_info
     *
     * @return $this
     */
    public function setAgentInfo($agent_info)
    {
        $this->container['agent_info'] = $agent_info;

        return $this;
    }

    /**
     * Gets supplier_info
     *
     * @return \Swagger\Client\Model\Ffd105SupplierInfo1
     */
    public function getSupplierInfo()
    {
        return $this->container['supplier_info'];
    }

    /**
     * Sets supplier_info
     *
     * @param \Swagger\Client\Model\Ffd105SupplierInfo1 $supplier_info supplier_info
     *
     * @return $this
     */
    public function setSupplierInfo($supplier_info)
    {
        $this->container['supplier_info'] = $supplier_info;

        return $this;
    }

    /**
     * Gets user_data
     *
     * @return string
     */
    public function getUserData()
    {
        return $this->container['user_data'];
    }

    /**
     * Sets user_data
     *
     * @param string $user_data Тег 1191. <br>Дополнительный реквизит предмета расчета.
     *
     * @return $this
     */
    public function setUserData($user_data)
    {
        $this->container['user_data'] = $user_data;

        return $this;
    }

    /**
     * Gets excise
     *
     * @return float
     */
    public function getExcise()
    {
        return $this->container['excise'];
    }

    /**
     * Sets excise
     *
     * @param float $excise Тег 1229. <br> Сумма акциза в рублях <br /> - целая часть не более 8 знаков; <br /> - дробная часть не более 2 знаков; <br /> - значение не может быть отрицательным;
     *
     * @return $this
     */
    public function setExcise($excise)
    {
        $this->container['excise'] = $excise;

        return $this;
    }

    /**
     * Gets country_code
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->container['country_code'];
    }

    /**
     * Sets country_code
     *
     * @param string $country_code Тег 1230. <br> Цифровой код страны происхождения товара: ровно 3 цифры. Если переданный код страны происхождения имеет длину меньше 3 цифр, то он дополняется справа пробелами.
     *
     * @return $this
     */
    public function setCountryCode($country_code)
    {
        $this->container['country_code'] = $country_code;

        return $this;
    }

    /**
     * Gets declaration_number
     *
     * @return string
     */
    public function getDeclarationNumber()
    {
        return $this->container['declaration_number'];
    }

    /**
     * Sets declaration_number
     *
     * @param string $declaration_number Тег 1231. <br>Номер таможенной декларации.
     *
     * @return $this
     */
    public function setDeclarationNumber($declaration_number)
    {
        $this->container['declaration_number'] = $declaration_number;

        return $this;
    }

    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed $value Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
