<?php
/**
 * Webhook
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PaySelection
 *
 * PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.
 *
 * OpenAPI spec version: v3-oas3
 * Contact: support@payselection.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.42
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * Webhook Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Webhook implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = 'Event';

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $swaggerModelName = 'Webhook';

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     */
    protected static $swaggerTypes = [
        'event' => 'string',
        'transaction_id' => '\Swagger\Client\Model\TransactionId',
        'order_id' => '\Swagger\Client\Model\OrderId',
        'amount' => '\Swagger\Client\Model\Amount',
        'currency' => '\Swagger\Client\Model\Currency',
        'service_id' => '\Swagger\Client\Model\ServiceId',
        'date_time' => '\Swagger\Client\Model\\DateTime',
        'is_test' => '\Swagger\Client\Model\IsTest',
        'email' => '\Swagger\Client\Model\Email',
        'phone' => '\Swagger\Client\Model\Phone',
        'description' => '\Swagger\Client\Model\Description',
        'custom_fields' => '\Swagger\Client\Model\CustomFields',
        'brand' => '\Swagger\Client\Model\Brand',
        'country_code_alpha2' => '\Swagger\Client\Model\CountryCodeAlpha2',
        'bank' => '\Swagger\Client\Model\Bank'
    ];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     */
    protected static $swaggerFormats = [
        'event' => null,
        'transaction_id' => null,
        'order_id' => null,
        'amount' => null,
        'currency' => null,
        'service_id' => null,
        'date_time' => null,
        'is_test' => null,
        'email' => null,
        'phone' => null,
        'description' => null,
        'custom_fields' => null,
        'brand' => null,
        'country_code_alpha2' => null,
        'bank' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'event' => 'Event',
        'transaction_id' => 'TransactionId',
        'order_id' => 'OrderId',
        'amount' => 'Amount',
        'currency' => 'Currency',
        'service_id' => 'Service_Id',
        'date_time' => 'DateTime',
        'is_test' => 'IsTest',
        'email' => 'Email',
        'phone' => 'Phone',
        'description' => 'Description',
        'custom_fields' => 'CustomFields',
        'brand' => 'Brand',
        'country_code_alpha2' => 'Country_Code_Alpha2',
        'bank' => 'Bank'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'event' => 'setEvent',
        'transaction_id' => 'setTransactionId',
        'order_id' => 'setOrderId',
        'amount' => 'setAmount',
        'currency' => 'setCurrency',
        'service_id' => 'setServiceId',
        'date_time' => 'setDateTime',
        'is_test' => 'setIsTest',
        'email' => 'setEmail',
        'phone' => 'setPhone',
        'description' => 'setDescription',
        'custom_fields' => 'setCustomFields',
        'brand' => 'setBrand',
        'country_code_alpha2' => 'setCountryCodeAlpha2',
        'bank' => 'setBank'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'event' => 'getEvent',
        'transaction_id' => 'getTransactionId',
        'order_id' => 'getOrderId',
        'amount' => 'getAmount',
        'currency' => 'getCurrency',
        'service_id' => 'getServiceId',
        'date_time' => 'getDateTime',
        'is_test' => 'getIsTest',
        'email' => 'getEmail',
        'phone' => 'getPhone',
        'description' => 'getDescription',
        'custom_fields' => 'getCustomFields',
        'brand' => 'getBrand',
        'country_code_alpha2' => 'getCountryCodeAlpha2',
        'bank' => 'getBank'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['event'] = isset($data['event']) ? $data['event'] : null;
        $this->container['transaction_id'] = isset($data['transaction_id']) ? $data['transaction_id'] : null;
        $this->container['order_id'] = isset($data['order_id']) ? $data['order_id'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['service_id'] = isset($data['service_id']) ? $data['service_id'] : null;
        $this->container['date_time'] = isset($data['date_time']) ? $data['date_time'] : null;
        $this->container['is_test'] = isset($data['is_test']) ? $data['is_test'] : null;
        $this->container['email'] = isset($data['email']) ? $data['email'] : null;
        $this->container['phone'] = isset($data['phone']) ? $data['phone'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['custom_fields'] = isset($data['custom_fields']) ? $data['custom_fields'] : null;
        $this->container['brand'] = isset($data['brand']) ? $data['brand'] : null;
        $this->container['country_code_alpha2'] = isset($data['country_code_alpha2']) ? $data['country_code_alpha2'] : null;
        $this->container['bank'] = isset($data['bank']) ? $data['bank'] : null;

        // Initialize discriminator property with the model name.
        $discriminator = array_search('Event', self::$attributeMap, true);
        $this->container[$discriminator] = static::$swaggerModelName;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['event'] === null) {
            $invalidProperties[] = "'event' can't be null";
        }
        if ($this->container['transaction_id'] === null) {
            $invalidProperties[] = "'transaction_id' can't be null";
        }
        if ($this->container['order_id'] === null) {
            $invalidProperties[] = "'order_id' can't be null";
        }
        if ($this->container['amount'] === null) {
            $invalidProperties[] = "'amount' can't be null";
        }
        if ($this->container['currency'] === null) {
            $invalidProperties[] = "'currency' can't be null";
        }
        if ($this->container['service_id'] === null) {
            $invalidProperties[] = "'service_id' can't be null";
        }
        if ($this->container['date_time'] === null) {
            $invalidProperties[] = "'date_time' can't be null";
        }
        if ($this->container['is_test'] === null) {
            $invalidProperties[] = "'is_test' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets event
     *
     * @return string
     */
    public function getEvent()
    {
        return $this->container['event'];
    }

    /**
     * Sets event
     *
     * @param string $event Event
     *
     * @return $this
     */
    public function setEvent($event)
    {
        $this->container['event'] = $event;

        return $this;
    }

    /**
     * Gets transaction_id
     *
     * @return \Swagger\Client\Model\TransactionId
     */
    public function getTransactionId()
    {
        return $this->container['transaction_id'];
    }

    /**
     * Sets transaction_id
     *
     * @param \Swagger\Client\Model\TransactionId $transaction_id transaction_id
     *
     * @return $this
     */
    public function setTransactionId($transaction_id)
    {
        $this->container['transaction_id'] = $transaction_id;

        return $this;
    }

    /**
     * Gets order_id
     *
     * @return \Swagger\Client\Model\OrderId
     */
    public function getOrderId()
    {
        return $this->container['order_id'];
    }

    /**
     * Sets order_id
     *
     * @param \Swagger\Client\Model\OrderId $order_id order_id
     *
     * @return $this
     */
    public function setOrderId($order_id)
    {
        $this->container['order_id'] = $order_id;

        return $this;
    }

    /**
     * Gets amount
     *
     * @return \Swagger\Client\Model\Amount
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     *
     * @param \Swagger\Client\Model\Amount $amount amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets currency
     *
     * @return \Swagger\Client\Model\Currency
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     *
     * @param \Swagger\Client\Model\Currency $currency currency
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets service_id
     *
     * @return \Swagger\Client\Model\ServiceId
     */
    public function getServiceId()
    {
        return $this->container['service_id'];
    }

    /**
     * Sets service_id
     *
     * @param \Swagger\Client\Model\ServiceId $service_id service_id
     *
     * @return $this
     */
    public function setServiceId($service_id)
    {
        $this->container['service_id'] = $service_id;

        return $this;
    }

    /**
     * Gets date_time
     *
     * @return \Swagger\Client\Model\\DateTime
     */
    public function getDateTime()
    {
        return $this->container['date_time'];
    }

    /**
     * Sets date_time
     *
     * @param \Swagger\Client\Model\\DateTime $date_time date_time
     *
     * @return $this
     */
    public function setDateTime($date_time)
    {
        $this->container['date_time'] = $date_time;

        return $this;
    }

    /**
     * Gets is_test
     *
     * @return \Swagger\Client\Model\IsTest
     */
    public function getIsTest()
    {
        return $this->container['is_test'];
    }

    /**
     * Sets is_test
     *
     * @param \Swagger\Client\Model\IsTest $is_test is_test
     *
     * @return $this
     */
    public function setIsTest($is_test)
    {
        $this->container['is_test'] = $is_test;

        return $this;
    }

    /**
     * Gets email
     *
     * @return \Swagger\Client\Model\Email
     */
    public function getEmail()
    {
        return $this->container['email'];
    }

    /**
     * Sets email
     *
     * @param \Swagger\Client\Model\Email $email email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->container['email'] = $email;

        return $this;
    }

    /**
     * Gets phone
     *
     * @return \Swagger\Client\Model\Phone
     */
    public function getPhone()
    {
        return $this->container['phone'];
    }

    /**
     * Sets phone
     *
     * @param \Swagger\Client\Model\Phone $phone phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->container['phone'] = $phone;

        return $this;
    }

    /**
     * Gets description
     *
     * @return \Swagger\Client\Model\Description
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     *
     * @param \Swagger\Client\Model\Description $description description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets custom_fields
     *
     * @return \Swagger\Client\Model\CustomFields
     */
    public function getCustomFields()
    {
        return $this->container['custom_fields'];
    }

    /**
     * Sets custom_fields
     *
     * @param \Swagger\Client\Model\CustomFields $custom_fields custom_fields
     *
     * @return $this
     */
    public function setCustomFields($custom_fields)
    {
        $this->container['custom_fields'] = $custom_fields;

        return $this;
    }

    /**
     * Gets brand
     *
     * @return \Swagger\Client\Model\Brand
     */
    public function getBrand()
    {
        return $this->container['brand'];
    }

    /**
     * Sets brand
     *
     * @param \Swagger\Client\Model\Brand $brand brand
     *
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->container['brand'] = $brand;

        return $this;
    }

    /**
     * Gets country_code_alpha2
     *
     * @return \Swagger\Client\Model\CountryCodeAlpha2
     */
    public function getCountryCodeAlpha2()
    {
        return $this->container['country_code_alpha2'];
    }

    /**
     * Sets country_code_alpha2
     *
     * @param \Swagger\Client\Model\CountryCodeAlpha2 $country_code_alpha2 country_code_alpha2
     *
     * @return $this
     */
    public function setCountryCodeAlpha2($country_code_alpha2)
    {
        $this->container['country_code_alpha2'] = $country_code_alpha2;

        return $this;
    }

    /**
     * Gets bank
     *
     * @return \Swagger\Client\Model\Bank
     */
    public function getBank()
    {
        return $this->container['bank'];
    }

    /**
     * Sets bank
     *
     * @param \Swagger\Client\Model\Bank $bank bank
     *
     * @return $this
     */
    public function setBank($bank)
    {
        $this->container['bank'] = $bank;

        return $this;
    }

    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed $value Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
