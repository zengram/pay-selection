<?php
/**
 * Configuration
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PaySelection
 *
 * PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.
 *
 * OpenAPI spec version: v3-oas3
 * Contact: support@payselection.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.42
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client;

/**
 * Configuration Class Doc Comment
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Configuration
{
    private static $defaultConfiguration;

    /**
     * Associate array to store API key(s)
     *
     * @var string[]
     */
    protected $apiKeys = [];

    /**
     * Associate array to store API prefix (e.g. Bearer)
     *
     * @var string[]
     */
    protected $apiKeyPrefixes = [];

    /**
     * Access token for OAuth
     *
     * @var string
     */
    protected $accessToken = '';

    /**
     * Username for HTTP basic authentication
     *
     * @var string
     */
    protected $username = '';

    /**
     * Password for HTTP basic authentication
     *
     * @var string
     */
    protected $password = '';

    /**
     * The host
     *
     * @var string
     */
    protected $host = 'https://gw.payselection.com/';

    /**
     * User agent of the HTTP request, set to "PHP-Swagger" by default
     *
     * @var string
     */
    protected $userAgent = 'Swagger-Codegen/1.0.0/php';

    /**
     * Debug switch (default set to false)
     *
     * @var bool
     */
    protected $debug = false;

    /**
     * Debug file location (log to STDOUT by default)
     *
     * @var string
     */
    protected $debugFile = 'php://output';

    /**
     * Debug file location (log to STDOUT by default)
     *
     * @var string
     */
    protected $tempFolderPath;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tempFolderPath = sys_get_temp_dir();
    }

    /**
     * Sets API key
     *
     * @param string $apiKeyIdentifier API key identifier (authentication scheme)
     * @param string $key API key or token
     *
     * @return $this
     */
    public function setApiKey($apiKeyIdentifier, $key)
    {
        $this->apiKeys[$apiKeyIdentifier] = $key;
        return $this;
    }

    /**
     * Gets API key
     *
     * @param string $apiKeyIdentifier API key identifier (authentication scheme)
     *
     * @return string API key or token
     */
    public function getApiKey($apiKeyIdentifier)
    {
        return isset($this->apiKeys[$apiKeyIdentifier]) ? $this->apiKeys[$apiKeyIdentifier] : null;
    }

    /**
     * Sets the prefix for API key (e.g. Bearer)
     *
     * @param string $apiKeyIdentifier API key identifier (authentication scheme)
     * @param string $prefix API key prefix, e.g. Bearer
     *
     * @return $this
     */
    public function setApiKeyPrefix($apiKeyIdentifier, $prefix)
    {
        $this->apiKeyPrefixes[$apiKeyIdentifier] = $prefix;
        return $this;
    }

    /**
     * Gets API key prefix
     *
     * @param string $apiKeyIdentifier API key identifier (authentication scheme)
     *
     * @return string
     */
    public function getApiKeyPrefix($apiKeyIdentifier)
    {
        return isset($this->apiKeyPrefixes[$apiKeyIdentifier]) ? $this->apiKeyPrefixes[$apiKeyIdentifier] : null;
    }

    /**
     * Sets the access token for OAuth
     *
     * @param string $accessToken Token for OAuth
     *
     * @return $this
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * Gets the access token for OAuth
     *
     * @return string Access token for OAuth
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Sets the username for HTTP basic authentication
     *
     * @param string $username Username for HTTP basic authentication
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Gets the username for HTTP basic authentication
     *
     * @return string Username for HTTP basic authentication
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the password for HTTP basic authentication
     *
     * @param string $password Password for HTTP basic authentication
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Gets the password for HTTP basic authentication
     *
     * @return string Password for HTTP basic authentication
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the host
     *
     * @param string $host Host
     *
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * Gets the host
     *
     * @return string Host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets the user agent of the api client
     *
     * @param string $userAgent the user agent of the api client
     *
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setUserAgent($userAgent)
    {
        if (!is_string($userAgent)) {
            throw new \InvalidArgumentException('User-agent must be a string.');
        }

        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * Gets the user agent of the api client
     *
     * @return string user agent
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Sets debug flag
     *
     * @param bool $debug Debug flag
     *
     * @return $this
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * Gets the debug flag
     *
     * @return bool
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * Sets the debug file
     *
     * @param string $debugFile Debug file
     *
     * @return $this
     */
    public function setDebugFile($debugFile)
    {
        $this->debugFile = $debugFile;
        return $this;
    }

    /**
     * Gets the debug file
     *
     * @return string
     */
    public function getDebugFile()
    {
        return $this->debugFile;
    }

    /**
     * Sets the temp folder path
     *
     * @param string $tempFolderPath Temp folder path
     *
     * @return $this
     */
    public function setTempFolderPath($tempFolderPath)
    {
        $this->tempFolderPath = $tempFolderPath;
        return $this;
    }

    /**
     * Gets the temp folder path
     *
     * @return string Temp folder path
     */
    public function getTempFolderPath()
    {
        return $this->tempFolderPath;
    }

    /**
     * Gets the default configuration instance
     *
     * @return Configuration
     */
    public static function getDefaultConfiguration()
    {
        if (self::$defaultConfiguration === null) {
            self::$defaultConfiguration = new Configuration();
        }

        return self::$defaultConfiguration;
    }

    /**
     * Sets the detault configuration instance
     *
     * @param Configuration $config An instance of the Configuration Object
     *
     * @return void
     */
    public static function setDefaultConfiguration(Configuration $config)
    {
        self::$defaultConfiguration = $config;
    }

    /**
     * Gets the essential information for debugging
     *
     * @return string The report for debugging
     */
    public static function toDebugReport()
    {
        $report = 'PHP SDK (Swagger\Client) Debug Report:' . PHP_EOL;
        $report .= '    OS: ' . php_uname() . PHP_EOL;
        $report .= '    PHP Version: ' . PHP_VERSION . PHP_EOL;
        $report .= '    OpenAPI Spec Version: v3-oas3' . PHP_EOL;
        $report .= '    Temp Folder Path: ' . self::getDefaultConfiguration()->getTempFolderPath() . PHP_EOL;

        return $report;
    }

    /**
     * Get API key (with prefix if set)
     *
     * @param string $apiKeyIdentifier name of apikey
     *
     * @return string API key with the prefix
     */
    public function getApiKeyWithPrefix($apiKeyIdentifier)
    {
        $prefix = $this->getApiKeyPrefix($apiKeyIdentifier);
        $apiKey = $this->getApiKey($apiKeyIdentifier);

        if ($apiKey === null) {
            return null;
        }

        if ($prefix === null) {
            $keyWithPrefix = $apiKey;
        } else {
            $keyWithPrefix = $prefix . ' ' . $apiKey;
        }

        return $keyWithPrefix;
    }
}
