<?php
/**
 * ObjectSerializer
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PaySelection
 *
 * PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.
 *
 * OpenAPI spec version: v3-oas3
 * Contact: support@payselection.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.42
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client;

/**
 * ObjectSerializer Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ObjectSerializer
{
    /**
     * Serialize data
     *
     * @param mixed $data the data to serialize
     * @param string $type the SwaggerType of the data
     * @param string $format the format of the Swagger type of the data
     *
     * @return string|object serialized form of $data
     */
    public static function sanitizeForSerialization($data, $type = null, $format = null)
    {
        if (is_scalar($data) || null === $data) {
            return $data;
        } elseif ($data instanceof \DateTime) {
            return ($format === 'date') ? $data->format('Y-m-d') : $data->format(\DateTime::ATOM);
        } elseif (is_array($data)) {
            foreach ($data as $property => $value) {
                $data[$property] = self::sanitizeForSerialization($value);
            }
            return $data;
        } elseif ($data instanceof \stdClass) {
            foreach ($data as $property => $value) {
                $data->$property = self::sanitizeForSerialization($value);
            }
            return $data;
        } elseif (is_object($data)) {
            $values = [];
            $formats = $data::swaggerFormats();
            foreach ($data::swaggerTypes() as $property => $swaggerType) {
                $getter = $data::getters()[$property];
                $value = $data->$getter();
                if ($value !== null
                    && !in_array(
                        $swaggerType,
                        [
                            'DateTime',
                            'bool',
                            'boolean',
                            'byte',
                            'double',
                            'float',
                            'int',
                            'integer',
                            'mixed',
                            'number',
                            'object',
                            'string',
                            'void'
                        ],
                        true
                    )
                    && method_exists($swaggerType, 'getAllowableEnumValues')
                    && !in_array($value, $swaggerType::getAllowableEnumValues(), true)) {
                    $imploded = implode("', '", $swaggerType::getAllowableEnumValues());
                    throw new \InvalidArgumentException(
                        "Invalid value for enum '$swaggerType', must be one of: '$imploded'"
                    );
                }
                if ($value !== null) {
                    $values[$data::attributeMap()[$property]] = self::sanitizeForSerialization(
                        $value,
                        $swaggerType,
                        $formats[$property]
                    );
                }
            }
            return (object)$values;
        } else {
            return (string)$data;
        }
    }

    /**
     * Sanitize filename by removing path.
     * e.g. ../../sun.gif becomes sun.gif
     *
     * @param string $filename filename to be sanitized
     *
     * @return string the sanitized filename
     */
    public static function sanitizeFilename($filename)
    {
        if (preg_match("/.*[\/\\\\](.*)$/", $filename, $match)) {
            return $match[1];
        } else {
            return $filename;
        }
    }

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the path, by url-encoding.
     *
     * @param string $value a string which will be part of the path
     *
     * @return string the serialized object
     */
    public static function toPathValue($value)
    {
        return rawurlencode(self::toString($value));
    }

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the query, by imploding comma-separated if it's an object.
     * If it's a string, pass through unchanged. It will be url-encoded
     * later.
     *
     * @param string[]|string|\DateTime $object an object to be serialized to a string
     *
     * @return string the serialized object
     */
    public static function toQueryValue($object)
    {
        if (is_array($object)) {
            return implode(',', $object);
        } else {
            return self::toString($object);
        }
    }

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the header. If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     *
     * @param string $value a string which will be part of the header
     *
     * @return string the header string
     */
    public static function toHeaderValue($value)
    {
        return self::toString($value);
    }

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the http body (form parameter). If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     *
     * @param string|\SplFileObject $value the value of the form parameter
     *
     * @return string the form string
     */
    public static function toFormValue($value)
    {
        if ($value instanceof \SplFileObject) {
            return $value->getRealPath();
        } else {
            return self::toString($value);
        }
    }

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the parameter. If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     *
     * @param string|\DateTime $value the value of the parameter
     *
     * @return string the header string
     */
    public static function toString($value)
    {
        if ($value instanceof \DateTime) { // datetime in ISO8601 format
            return $value->format(\DateTime::ATOM);
        } else {
            return $value;
        }
    }

    /**
     * Serialize an array to a string.
     *
     * @param array $collection collection to serialize to a string
     * @param string $collectionFormat the format use for serialization (csv,
     * ssv, tsv, pipes, multi)
     * @param bool $allowCollectionFormatMulti allow collection format to be a multidimensional array
     *
     * @return string
     */
    public static function serializeCollection(
        array $collection,
        $collectionFormat,
        $allowCollectionFormatMulti = false
    ) {
        if ($allowCollectionFormatMulti && ('multi' === $collectionFormat)) {
            // http_build_query() almost does the job for us. We just
            // need to fix the result of multidimensional arrays.
            return preg_replace('/%5B[0-9]+%5D=/', '=', http_build_query($collection, '', '&'));
        }
        switch ($collectionFormat) {
            case 'pipes':
                return implode('|', $collection);

            case 'tsv':
                return implode("\t", $collection);

            case 'ssv':
                return implode(' ', $collection);

            case 'csv':
                // Deliberate fall through. CSV is default format.
            default:
                return implode(',', $collection);
        }
    }

    /**
     * Deserialize a JSON string into an object
     *
     * @param mixed $data object or primitive to be deserialized
     * @param string $class class name is passed as a string
     * @param string[] $httpHeaders HTTP headers
     * @param string $discriminator discriminator if polymorphism is used
     *
     * @return object|array|null an single or an array of $class instances
     */
    public static function deserialize($data, $class, $httpHeaders = null)
    {
        if (null === $data) {
            return null;
        } elseif (substr($class, 0, 4) === 'map[') { // for associative array e.g. map[string,int]
            $inner = substr($class, 4, -1);
            $deserialized = [];
            if (strrpos($inner, ",") !== false) {
                $subClass_array = explode(',', $inner, 2);
                $subClass = $subClass_array[1];
                foreach ($data as $key => $value) {
                    $deserialized[$key] = self::deserialize($value, $subClass, null);
                }
            }
            return $deserialized;
        } elseif (strcasecmp(substr($class, -2), '[]') === 0) {
            $subClass = substr($class, 0, -2);
            $values = [];
            foreach ($data as $key => $value) {
                $values[] = self::deserialize($value, $subClass, null);
            }
            return $values;
        } elseif ($class === 'object') {
            settype($data, 'array');
            return $data;
        } elseif ($class === '\DateTime') {
            // Some API's return an invalid, empty string as a
            // date-time property. DateTime::__construct() will return
            // the current time for empty input which is probably not
            // what is meant. The invalid empty string is probably to
            // be interpreted as a missing field/value. Let's handle
            // this graceful.
            if (!empty($data)) {
                return new \DateTime($data);
            } else {
                return null;
            }
        } elseif (in_array(
            $class,
            [
                'DateTime',
                'bool',
                'boolean',
                'byte',
                'double',
                'float',
                'int',
                'integer',
                'mixed',
                'number',
                'object',
                'string',
                'void'
            ],
            true
        )) {
            settype($data, $class);
            return $data;
        } elseif ($class === '\SplFileObject') {
            /** @var \Psr\Http\Message\StreamInterface $data */

            // determine file name
            if (array_key_exists('Content-Disposition', $httpHeaders) &&
                preg_match(
                    '/inline; filename=[\'"]?([^\'"\s]+)[\'"]?$/i',
                    $httpHeaders['Content-Disposition'],
                    $match
                )) {
                $filename = Configuration::getDefaultConfiguration()->getTempFolderPath(
                    ) . DIRECTORY_SEPARATOR . self::sanitizeFilename($match[1]);
            } else {
                $filename = tempnam(Configuration::getDefaultConfiguration()->getTempFolderPath(), '');
            }

            $file = fopen($filename, 'w');
            while ($chunk = $data->read(200)) {
                fwrite($file, $chunk);
            }
            fclose($file);

            return new \SplFileObject($filename, 'r');
        } elseif (method_exists($class, 'getAllowableEnumValues')) {
            if (!in_array($data, $class::getAllowableEnumValues(), true)) {
                $imploded = implode("', '", $class::getAllowableEnumValues());
                throw new \InvalidArgumentException("Invalid value for enum '$class', must be one of: '$imploded'");
            }
            return $data;
        } else {
            // If a discriminator is defined and points to a valid subclass, use it.
            $discriminator = $class::DISCRIMINATOR;
            if (!empty($discriminator) && isset($data->{$discriminator}) && is_string($data->{$discriminator})) {
                $subclass = '{{invokerPackage}}\Model\\' . $data->{$discriminator};
                if (is_subclass_of($subclass, $class)) {
                    $class = $subclass;
                }
            }
            $instance = new $class();
            foreach ($instance::swaggerTypes() as $property => $type) {
                $propertySetter = $instance::setters()[$property];

                if (!isset($propertySetter) || !isset($data->{$instance::attributeMap()[$property]})) {
                    continue;
                }

                $propertyValue = $data->{$instance::attributeMap()[$property]};
                if (isset($propertyValue)) {
                    $instance->$propertySetter(self::deserialize($propertyValue, $type, null));
                }
            }
            return $instance;
        }
    }
}
