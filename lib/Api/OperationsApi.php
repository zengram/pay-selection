<?php
/**
 * OperationsApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PaySelection
 *
 * PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.
 *
 * OpenAPI spec version: v3-oas3
 * Contact: support@payselection.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.42
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Swagger\Client\ApiException;
use Swagger\Client\Configuration;
use Swagger\Client\HeaderSelector;
use Swagger\Client\ObjectSerializer;

/**
 * OperationsApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class OperationsApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @param ClientInterface $client
     * @param Configuration $config
     * @param HeaderSelector $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation balance
     *
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\BalanceResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function balance($x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->balanceWithHttpInfo($x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation balanceWithHttpInfo
     *
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\BalanceResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function balanceWithHttpInfo($x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\BalanceResponse';
        $request = $this->balanceRequest($x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\BalanceResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation balanceAsync
     *
     *
     *
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function balanceAsync($x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->balanceAsyncWithHttpInfo($x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation balanceAsyncWithHttpInfo
     *
     *
     *
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function balanceAsyncWithHttpInfo($x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\BalanceResponse';
        $request = $this->balanceRequest($x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'balance'
     *
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function balanceRequest($x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling balance'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling balance'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling balance'
            );
        }

        $resourcePath = '/balance';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation block
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function block($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->blockWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation blockWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function blockWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->blockRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation blockAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function blockAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->blockAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation blockAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function blockAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->blockRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'block'
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function blockRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling block'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling block'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling block'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling block'
            );
        }

        $resourcePath = '/payments/requests/block';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation cancel
     *
     * @param \Swagger\Client\Model\PaymentCancelRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function cancel($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->cancelWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation cancelWithHttpInfo
     *
     * @param \Swagger\Client\Model\PaymentCancelRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function cancelWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->cancelRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation cancelAsync
     *
     *
     *
     * @param \Swagger\Client\Model\PaymentCancelRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function cancelAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->cancelAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation cancelAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\PaymentCancelRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function cancelAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->cancelRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'cancel'
     *
     * @param \Swagger\Client\Model\PaymentCancelRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function cancelRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling cancel'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling cancel'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling cancel'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling cancel'
            );
        }

        $resourcePath = '/payments/cancellation';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation charge
     *
     * @param \Swagger\Client\Model\PaymentChargeRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function charge($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->chargeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation chargeWithHttpInfo
     *
     * @param \Swagger\Client\Model\PaymentChargeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function chargeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->chargeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation chargeAsync
     *
     *
     *
     * @param \Swagger\Client\Model\PaymentChargeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function chargeAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->chargeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation chargeAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\PaymentChargeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function chargeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->chargeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'charge'
     *
     * @param \Swagger\Client\Model\PaymentChargeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function chargeRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling charge'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling charge'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling charge'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling charge'
            );
        }

        $resourcePath = '/payments/charge';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation onfirm
     *
     * @param \Swagger\Client\Model\ConfirmationRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\InlineResponse201
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function onfirm($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->onfirmWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation onfirmWithHttpInfo
     *
     * @param \Swagger\Client\Model\ConfirmationRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\InlineResponse201, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function onfirmWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\InlineResponse201';
        $request = $this->onfirmRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\InlineResponse201',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation onfirmAsync
     *
     *
     *
     * @param \Swagger\Client\Model\ConfirmationRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function onfirmAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->onfirmAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation onfirmAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\ConfirmationRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function onfirmAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\InlineResponse201';
        $request = $this->onfirmRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'onfirm'
     *
     * @param \Swagger\Client\Model\ConfirmationRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function onfirmRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling onfirm'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling onfirm'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling onfirm'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling onfirm'
            );
        }

        $resourcePath = '/payments/confirmation';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation ordersOrderId
     *
     * @param string $order_id order_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\TransactionIdResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function ordersOrderId($order_id, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->ordersOrderIdWithHttpInfo($order_id, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation ordersOrderIdWithHttpInfo
     *
     * @param string $order_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\TransactionIdResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function ordersOrderIdWithHttpInfo($order_id, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\TransactionIdResponse';
        $request = $this->ordersOrderIdRequest($order_id, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\TransactionIdResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation ordersOrderIdAsync
     *
     *
     *
     * @param string $order_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function ordersOrderIdAsync($order_id, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->ordersOrderIdAsyncWithHttpInfo($order_id, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation ordersOrderIdAsyncWithHttpInfo
     *
     *
     *
     * @param string $order_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function ordersOrderIdAsyncWithHttpInfo($order_id, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\TransactionIdResponse';
        $request = $this->ordersOrderIdRequest($order_id, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'ordersOrderId'
     *
     * @param string $order_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function ordersOrderIdRequest($order_id, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'order_id' is set
        if ($order_id === null || (is_array($order_id) && count($order_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $order_id when calling ordersOrderId'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling ordersOrderId'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling ordersOrderId'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling ordersOrderId'
            );
        }

        $resourcePath = '/orders/{OrderId}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }

        // path params
        if ($order_id !== null) {
            $resourcePath = str_replace(
                '{' . 'OrderId' . '}',
                ObjectSerializer::toPathValue($order_id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation pay
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function pay($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->payWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation payWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function payWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->payRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation payAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function payAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->payAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation payAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function payAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->payRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'pay'
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function payRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling pay'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling pay'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling pay'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling pay'
            );
        }

        $resourcePath = '/payments/requests/single';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation payout
     *
     * @param \Swagger\Client\Model\NewPayoutRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function payout($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->payoutWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation payoutWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewPayoutRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function payoutWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->payoutRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation payoutAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewPayoutRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function payoutAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->payoutAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation payoutAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewPayoutRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function payoutAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->payoutRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'payout'
     *
     * @param \Swagger\Client\Model\NewPayoutRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function payoutRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling payout'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling payout'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling payout'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling payout'
            );
        }

        $resourcePath = '/payouts';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation publicPay
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     *
     * @return \Swagger\Client\Model\NewPublicPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function publicPay($body, $x_site_id, $x_request_id)
    {
        list($response) = $this->publicPayWithHttpInfo($body, $x_site_id, $x_request_id);
        return $response;
    }

    /**
     * Operation publicPayWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     *
     * @return array of \Swagger\Client\Model\NewPublicPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function publicPayWithHttpInfo($body, $x_site_id, $x_request_id)
    {
        $returnType = '\Swagger\Client\Model\NewPublicPaymentResponse';
        $request = $this->publicPayRequest($body, $x_site_id, $x_request_id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPublicPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation publicPayAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function publicPayAsync($body, $x_site_id, $x_request_id)
    {
        return $this->publicPayAsyncWithHttpInfo($body, $x_site_id, $x_request_id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation publicPayAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function publicPayAsyncWithHttpInfo($body, $x_site_id, $x_request_id)
    {
        $returnType = '\Swagger\Client\Model\NewPublicPaymentResponse';
        $request = $this->publicPayRequest($body, $x_site_id, $x_request_id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'publicPay'
     *
     * @param \Swagger\Client\Model\NewPaymentRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function publicPayRequest($body, $x_site_id, $x_request_id)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling publicPay'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling publicPay'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling publicPay'
            );
        }

        $resourcePath = '/payments/requests/public';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation rebill
     *
     * @param \Swagger\Client\Model\RebillRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function rebill($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->rebillWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation rebillWithHttpInfo
     *
     * @param \Swagger\Client\Model\RebillRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function rebillWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->rebillRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation rebillAsync
     *
     *
     *
     * @param \Swagger\Client\Model\RebillRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function rebillAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->rebillAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation rebillAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\RebillRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function rebillAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->rebillRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'rebill'
     *
     * @param \Swagger\Client\Model\RebillRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function rebillRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling rebill'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling rebill'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling rebill'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling rebill'
            );
        }

        $resourcePath = '/payments/requests/rebill';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation recurring
     *
     * @param \Swagger\Client\Model\NewRecurringRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewRecurringResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurring($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->recurringWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation recurringWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewRecurringRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewRecurringResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringResponse';
        $request = $this->recurringRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewRecurringResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalseRecurringResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation recurringAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewRecurringRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->recurringAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation recurringAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewRecurringRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringResponse';
        $request = $this->recurringRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'recurring'
     *
     * @param \Swagger\Client\Model\NewRecurringRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function recurringRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling recurring'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling recurring'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling recurring'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling recurring'
            );
        }

        $resourcePath = '/payments/recurring';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation recurringChange
     *
     * @param \Swagger\Client\Model\NewRecurringChangeRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewRecurringChangeResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringChange($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->recurringChangeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation recurringChangeWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewRecurringChangeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewRecurringChangeResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringChangeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringChangeResponse';
        $request = $this->recurringChangeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewRecurringChangeResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalseRecurringResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation recurringChangeAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewRecurringChangeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringChangeAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->recurringChangeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation recurringChangeAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewRecurringChangeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringChangeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringChangeResponse';
        $request = $this->recurringChangeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'recurringChange'
     *
     * @param \Swagger\Client\Model\NewRecurringChangeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function recurringChangeRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling recurringChange'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling recurringChange'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling recurringChange'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling recurringChange'
            );
        }

        $resourcePath = '/payments/recurring/change';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation recurringSearch
     *
     * @param \Swagger\Client\Model\RecurringSearchBody $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewRecurringSearchResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringSearch($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->recurringSearchWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation recurringSearchWithHttpInfo
     *
     * @param \Swagger\Client\Model\RecurringSearchBody $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewRecurringSearchResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringSearchWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringSearchResponse';
        $request = $this->recurringSearchRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewRecurringSearchResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalseRecurringResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation recurringSearchAsync
     *
     *
     *
     * @param \Swagger\Client\Model\RecurringSearchBody $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringSearchAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->recurringSearchAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation recurringSearchAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\RecurringSearchBody $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringSearchAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringSearchResponse';
        $request = $this->recurringSearchRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'recurringSearch'
     *
     * @param \Swagger\Client\Model\RecurringSearchBody $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function recurringSearchRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling recurringSearch'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling recurringSearch'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling recurringSearch'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling recurringSearch'
            );
        }

        $resourcePath = '/payments/recurring/search';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation recurringUnsubscribe
     *
     * @param \Swagger\Client\Model\NewRecurringUnsubscribeRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewRecurringUnsubscribeResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringUnsubscribe($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->recurringUnsubscribeWithHttpInfo(
            $body,
            $x_site_id,
            $x_request_id,
            $x_request_signature
        );
        return $response;
    }

    /**
     * Operation recurringUnsubscribeWithHttpInfo
     *
     * @param \Swagger\Client\Model\NewRecurringUnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewRecurringUnsubscribeResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function recurringUnsubscribeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringUnsubscribeResponse';
        $request = $this->recurringUnsubscribeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewRecurringUnsubscribeResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalseRecurringResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\DoublePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation recurringUnsubscribeAsync
     *
     *
     *
     * @param \Swagger\Client\Model\NewRecurringUnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringUnsubscribeAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->recurringUnsubscribeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation recurringUnsubscribeAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\NewRecurringUnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function recurringUnsubscribeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewRecurringUnsubscribeResponse';
        $request = $this->recurringUnsubscribeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'recurringUnsubscribe'
     *
     * @param \Swagger\Client\Model\NewRecurringUnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function recurringUnsubscribeRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling recurringUnsubscribe'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling recurringUnsubscribe'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling recurringUnsubscribe'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling recurringUnsubscribe'
            );
        }

        $resourcePath = '/payments/recurring/unsubscribe';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation refund
     *
     * @param \Swagger\Client\Model\RefundRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewPaymentResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function refund($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->refundWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation refundWithHttpInfo
     *
     * @param \Swagger\Client\Model\RefundRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewPaymentResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function refundWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->refundRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewPaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\FalsePaymentResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation refundAsync
     *
     *
     *
     * @param \Swagger\Client\Model\RefundRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function refundAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->refundAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation refundAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\RefundRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function refundAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewPaymentResponse';
        $request = $this->refundRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'refund'
     *
     * @param \Swagger\Client\Model\RefundRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function refundRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling refund'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling refund'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling refund'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling refund'
            );
        }

        $resourcePath = '/payments/refund';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation transactionstransactionId
     *
     * @param string $transaction_id transaction_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции) (required)
     * @param string $x_request_auth Тип авторизации (optional)
     *
     * @return \Swagger\Client\Model\TransactionIdResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function transactionstransactionId(
        $transaction_id,
        $x_site_id,
        $x_request_id,
        $x_request_signature,
        $x_request_auth = null
    ) {
        list($response) = $this->transactionstransactionIdWithHttpInfo(
            $transaction_id,
            $x_site_id,
            $x_request_id,
            $x_request_signature,
            $x_request_auth
        );
        return $response;
    }

    /**
     * Operation transactionstransactionIdWithHttpInfo
     *
     * @param string $transaction_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции) (required)
     * @param string $x_request_auth Тип авторизации (optional)
     *
     * @return array of \Swagger\Client\Model\TransactionIdResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function transactionstransactionIdWithHttpInfo(
        $transaction_id,
        $x_site_id,
        $x_request_id,
        $x_request_signature,
        $x_request_auth = null
    ) {
        $returnType = '\Swagger\Client\Model\TransactionIdResponse';
        $request = $this->transactionstransactionIdRequest(
            $transaction_id,
            $x_site_id,
            $x_request_id,
            $x_request_signature,
            $x_request_auth
        );

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\TransactionIdResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation transactionstransactionIdAsync
     *
     *
     *
     * @param string $transaction_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции) (required)
     * @param string $x_request_auth Тип авторизации (optional)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function transactionstransactionIdAsync(
        $transaction_id,
        $x_site_id,
        $x_request_id,
        $x_request_signature,
        $x_request_auth = null
    ) {
        return $this->transactionstransactionIdAsyncWithHttpInfo(
            $transaction_id,
            $x_site_id,
            $x_request_id,
            $x_request_signature,
            $x_request_auth
        )
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation transactionstransactionIdAsyncWithHttpInfo
     *
     *
     *
     * @param string $transaction_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции) (required)
     * @param string $x_request_auth Тип авторизации (optional)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function transactionstransactionIdAsyncWithHttpInfo(
        $transaction_id,
        $x_site_id,
        $x_request_id,
        $x_request_signature,
        $x_request_auth = null
    ) {
        $returnType = '\Swagger\Client\Model\TransactionIdResponse';
        $request = $this->transactionstransactionIdRequest(
            $transaction_id,
            $x_site_id,
            $x_request_id,
            $x_request_signature,
            $x_request_auth
        );

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'transactionstransactionId'
     *
     * @param string $transaction_id (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции) (required)
     * @param string $x_request_auth Тип авторизации (optional)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function transactionstransactionIdRequest(
        $transaction_id,
        $x_site_id,
        $x_request_id,
        $x_request_signature,
        $x_request_auth = null
    ) {
        // verify the required parameter 'transaction_id' is set
        if ($transaction_id === null || (is_array($transaction_id) && count($transaction_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $transaction_id when calling transactionstransactionId'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling transactionstransactionId'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling transactionstransactionId'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling transactionstransactionId'
            );
        }

        $resourcePath = '/transactions/{transactionId}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }
        // header params
        if ($x_request_auth !== null) {
            $headerParams['X-REQUEST-AUTH'] = ObjectSerializer::toHeaderValue($x_request_auth);
        }

        // path params
        if ($transaction_id !== null) {
            $resourcePath = str_replace(
                '{' . 'transactionId' . '}',
                ObjectSerializer::toPathValue($transaction_id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation unsubscribe
     *
     * @param \Swagger\Client\Model\UnsubscribeRequest $body body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \Swagger\Client\Model\NewUnsubscribeResponse
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function unsubscribe($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        list($response) = $this->unsubscribeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature);
        return $response;
    }

    /**
     * Operation unsubscribeWithHttpInfo
     *
     * @param \Swagger\Client\Model\UnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return array of \Swagger\Client\Model\NewUnsubscribeResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \InvalidArgumentException
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function unsubscribeWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewUnsubscribeResponse';
        $request = $this->unsubscribeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\NewUnsubscribeResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation unsubscribeAsync
     *
     *
     *
     * @param \Swagger\Client\Model\UnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function unsubscribeAsync($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        return $this->unsubscribeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation unsubscribeAsyncWithHttpInfo
     *
     *
     *
     * @param \Swagger\Client\Model\UnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     * @throws \InvalidArgumentException
     */
    public function unsubscribeAsyncWithHttpInfo($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        $returnType = '\Swagger\Client\Model\NewUnsubscribeResponse';
        $request = $this->unsubscribeRequest($body, $x_site_id, $x_request_id, $x_request_signature);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'unsubscribe'
     *
     * @param \Swagger\Client\Model\UnsubscribeRequest $body (required)
     * @param string $x_site_id Идентификатор ТСП (required)
     * @param string $x_request_id Уникальный идентификатор запроса ТСП. (required)
     * @param string $x_request_signature Request signature. [description](#section/Request-signature) (required)
     *
     * @return \GuzzleHttp\Psr7\Request
     * @throws \InvalidArgumentException
     */
    protected function unsubscribeRequest($body, $x_site_id, $x_request_id, $x_request_signature)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling unsubscribe'
            );
        }
        // verify the required parameter 'x_site_id' is set
        if ($x_site_id === null || (is_array($x_site_id) && count($x_site_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_site_id when calling unsubscribe'
            );
        }
        // verify the required parameter 'x_request_id' is set
        if ($x_request_id === null || (is_array($x_request_id) && count($x_request_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_id when calling unsubscribe'
            );
        }
        // verify the required parameter 'x_request_signature' is set
        if ($x_request_signature === null || (is_array($x_request_signature) && count($x_request_signature) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $x_request_signature when calling unsubscribe'
            );
        }

        $resourcePath = '/payments/unsubscribe';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($x_site_id !== null) {
            $headerParams['X-SITE-ID'] = ObjectSerializer::toHeaderValue($x_site_id);
        }
        // header params
        if ($x_request_id !== null) {
            $headerParams['X-REQUEST-ID'] = ObjectSerializer::toHeaderValue($x_request_id);
        }
        // header params
        if ($x_request_signature !== null) {
            $headerParams['X-REQUEST-SIGNATURE'] = ObjectSerializer::toHeaderValue($x_request_signature);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);
            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @return array of http client options
     * @throws \RuntimeException on file opening failure
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
