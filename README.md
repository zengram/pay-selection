# SwaggerClient-php
PaySelection API specification   # Request signature   Все виды сообщений содержат параметр X-REQUEST-SIGNATURE в HTTP-заголовке, который содержит тестовое значение запроса, вычисленное с использованием алгоритма HMAC. При реализации проверки сообщений, обратите внимание, на следующие моменты:      Сообщение содержит:          1 Reguest method       2 URL       3 X-SITE-ID       4 X-REQUEST-ID       5 Request body      Пример:          POST       /payments/requests/single       16       1qazxsw23edc       {\"Amount\": \"123\", \"Currency\": \"RUB\", \"ExtraData\": {\"custom\": \"field\", \"key\": \"value\"}, \"CustomerInfo\": {\"Address\": \"string\", \"Country\": \"string\", \"Email\": \"string\", \"Language\": \"string\", \"Phone\": \"string\", \"Town\": \"string\", \"ZIP\": \"string\"}, \"Description\": \"string\", \"OrderId\": \"string\", \"PaymentMethod\": \"card\", \"PaymentDetails\": {\"CardholderName\": \"string\", \"CardNumber\": \"4111111111111111\", \"CVC\": \"987\", \"ExpMonth\": \"12\", \"ExpYear\": \"22\"}, \"RebillFlag\": true}              В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре:          \"5145fec78e5db15c51a83e520b61609e8bd934c912614be71ae3840a60e3b013\"       Сайт для онлайн формирования подписи:          http://beautifytools.com/hmac-generator.php     Пример формирования X-REQUEST-SIGNATURE (Python):          import hashlib       import hmac        def calc_signature(body: str, site_secret_value: str) -> str:         signatire = hmac.new(           key=site_secret_value.encode(),           msg=body.encode(),           digestmod=hashlib.sha256,         )         return signature.hexdigest()                 Пример формирования Signature  (PHP):               function getSignature($body, $secretKey)           {               $hash = hash_hmac('sha256', $body, $secretKey, false);               return $hash;           }   # Webhook signature    X-WEBHOOK-SIGNATURE вычисляется по тем же правилам, что и [Request signature](#section/Request-signature), за исключением X-REQUEST-ID.      Сообщение содержит:          1 Reguest method       2 URL (Notification URL to which WEBHOOK is sent)       3 X-SITE-ID       4 Request body      Пример:          POST       https://webhook.site/notification/       16       {\"Event\": \"Payment\", \"TransactionId\": \"PS00000000000007\", \"OrderId\": \"Ilya test\", \"Amount\": \"152.12\", \"Currency\": \"RUB\", \"DateTime\": \"16.09.2019 16.52.41\", \"IsTest\": 1, \"Email\": \"test@payselection.com\", \"Phone\": \"+1234567567890\", \"Description\": \"Test transaction\", \"CardMasked\": \"411111******1111\", \"CardHolder\": \"test card\", \"RebillId\": \"PS00000000000007\", \"ExpirationDate\": \"01/20\"}      В качестве ключа используется значение параметра, например:          \"sk16\"       Хэш вычисляется функцией SHA256 (получаем данные в шестнадцатеричной (!) кодировке в нижнем регистре):          \"a9cf187993620b64dd0551b1cae88119dd5bddf92ab18d830e5fce1c703cebcd\"                  # Webhook для проверки      После ввода клиентом карточных данных вы можете получить webhook с данными из запроса для сопоставления их с заказом. Для настройки обратитесь в тех.поддержку: необходимо сообщить url приемника и какие параметры из запроса требуется передать для проверки. Ваш сервис приема должен отдавать 200 статус, если оплату можно продолжать, или любой из 4хх и 5хх статусов для прерывания оплаты. Webhook можно настроить в личном кабинете→ Сервис→ действие(редактировать) → URL оповещения → сохранить. <b>Изменения вступят в силу в течении 15 мин.</b>      # Тестирование      Тестовые карты:        * 5375437783733009 3DS SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2175005001632383 3DS FAIL PAYMENT, FAIL PAYOUT     * 4113418297706145 non3DS SUССESS PAYMENT, SUCCESS PAYOUT, FAIL REBILL     * 4635224506614503 non3DS FAIL PAYMENT, FAIL PAYOUT     * 5260111696757102 3DS Redirect SUССESS PAYMENT, SUССESS PAYOUT, SUCCESS REBILL     * 2408684917843810 3DS Redirect FAIL PAYMENT, FAIL PAYOUT   # Widget  Для подключения виджета необходимо прописать на сайте скрипт в раздел **head**: ```javascript <script type=\"text/javascript\" src=\"https://widget.payselection.com/lib/pay-widget.js\"></script> ```   Для появления платежной формы необходимо зарегистрировать функцию для вызова метода pay:     ```javascript  <script type=\"text/javascript\">   this.pay = function() {     var widget = new pw.PayWidget();     console.log(\"PAY\");     widget.pay(     {       // serviceId - Идентификатор ТСП       serviceId: \"1410\",       // key - public key из личного кабинета мерчанта       key: \"0423x039a8a049xxxxxxxxxxx44653b1aa980b28\",       // logger -  для включения расширенного логирования при отладке       logger:true,     },       // Запрос с минимальным количеством параметров       {         MetaData: {           PaymentType: \"Pay\",         },         PaymentRequest: {           OrderId: \"string12\",           Amount: \"123\",           Currency: \"RUB\",           Description: \"string\",         },       },       // Запрос с максимальным количеством параметров       // См. запрос Create       {         // Варианты ключей которые могут приходить по колбекам:         // для onSuccess -> PAY_WIDGET:TRANSACTION_SUCCESS, PAY_WIDGET:CLOSE_AFTER_SUCCESS         // для onError -> PAY_WIDGET:TRANSACTION_FAIL, PAY_WIDGET:CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR, PAY_WIDGET:CLOSE_AFTER_FAIL,PAY_WIDGET:CLOSE_AFTER_CREATE_NETWORK_ERROR, PAY_WIDGET:CREATE_BAD_REQUEST_ERROR            // для onClose -> PAY_WIDGET:CLOSE_BEFORE_PAY         onSuccess: function(res) {           console.log(\"onSuccess from shop\", res);         },         onError: function(res) {           console.log(\"onFail from shop\", res);         },         onClose: function(res) {           console.log(\"onClose from shop\", res);         },       },       {only2Level: true} // необязательный параметр, необходим для корректной работы при наличии сайтов с поддоменами     );   }; </script> ```   И прописать вызов функции на событие, например, нажатие кнопки «Оплатить»:  ```javascript  $('#checkout').click(pay); ``` **Варианты настройки Return Urls для Виджета:** 1) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться)  ```javascript  window.location.href = res.returnUrl; ``` 2) Производится возврат на returnUrl из extraData или сервиса (но если returnUrl там не указаны, то виджет при инициации закрытия будет просто закрываться) ```javascript  if (res.returnUrl)   window.location.href = res.returnUrl; ```       3) Производится возврат на returnUrl из extraData  или сервиса (но если returnUrl там не указаны, то виджет будет перенаправлять по указанному в скрипте returnUrl) ```javascript  window.location.href = res.returnUrl || \"https://payselection.com/\" ``` # Cryptogram  Криптограмма — это идентификатор, представляющий собой случайную последовательность, ассоциированную с определённой платёжной картой.  Формирование криптограмм выполняется на основании данных платёжных карт пользователей.  Сформированная криптограмма используется в методах [Pay](#operation/Pay) и [Block](#operation/Block)    Скрипт для криптографирования карточных данных представлен ниже, где rawPubKey - public key из личного кабинета мерчанта:   ```javascript     var eccrypto = require(\"eccrypto\");      var rawPubKey = '0405397f7577bd835210a57708aafe876786dc8e2d12e6880917d61a4ad1d03a75068ea6bc26554c7a1bf5b50ed40105837eee001178579279eca57f89bdff5fc2'     var pubkey = Buffer.from(rawPubKey, 'hex');     eccrypto.encrypt(pubkey, Buffer(JSON.stringify({         \"TransactionDetails\": {             \"Amount\": 100,             \"Currency\": \"RUB\"         },         \"PaymentMethod\": \"Card\",         \"PaymentDetails\": {             \"CardholderName\":\"TEST CARD\",             \"CardNumber\":\"4111111111111111\",             \"CVC\":\"123\",             \"ExpMonth\":\"12\",             \"ExpYear\":\"24\"          },         \"MessageExpiration\": Date.now()+86400000,  //24 hours     })))     .then((encrypted) => {       var sendData = {           \"signedMessage\": JSON.stringify(               {                   \"encryptedMessage\": encrypted.ciphertext.toString(\"base64\"),                   \"ephemeralPublicKey\": encrypted.ephemPublicKey.toString(\"base64\")               }           ),           \"iv\": encrypted.iv.toString(\"base64\"),           \"tag\": encrypted.mac.toString(\"base64\")         };       var finalString = window.btoa(JSON.stringify(sendData));       console.log(finalString)     }   )    ``` # Аутентификация покупателя Если требуется 3-D Secure аутентификация (получен статус wait_for_3ds), в ответе на запрос статуса добавляется объект **StateDetails** с полями:  * **AcsUrl** — URL сервера аутентификации 3-D Secure, для перенаправления на страницу подтверждения от эмитента; * **PaReq** — зашифрованный запрос на аутентификацию 3-D Secure; * **MD** - уникальный идентификатор транзакции.  Для дополнительной проверки у эмитента выполните POST-запрос на URL сервера аутентификации 3-D Secure с параметрами:  * **TermUrl** — URL перенаправления покупателя после успешной аутентификации 3-D Secure; * **MD** — уникальный идентификатор транзакции; * **PaReq** — значение параметра pareq из ответа на платежный запрос.  Далее информация о покупателе передаётся в платежную систему карты. Банк-эмитент либо предоставляет разрешение на списание средств без аутентификации (frictionless flow), либо принимает решение о необходимости аутентификации с помощью одноразового пароля (challenge flow). После прохождения проверки покупатель перенаправляется по адресу TermUrl с зашифрованным результатом проверки в параметре PaRes.  При использовании карт, поддерживающих протокол 3-D Secure 2.0, механизм аутентификации остается тот же, за исключением необходимости выполнения метода Confirm в случае frictionless flow. Метод Confirm следует выполнять только при получении PaRes на TermUrl, также рекомендуется запросить статус транзакции, чтобы определить необходимость выполнения Confirm.

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: v3-oas3
- Build package: io.swagger.codegen.v3.generators.php.PhpClientCodegen

## Requirements

PHP 5.5 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://github.com/git_user_id/git_repo_id.git"
    }
  ],
  "require": {
    "git_user_id/git_repo_id": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/SwaggerClient-php/vendor/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->balance($x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->balance: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPaymentRequest(); // \Swagger\Client\Model\NewPaymentRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->block($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->block: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\PaymentCancelRequest(); // \Swagger\Client\Model\PaymentCancelRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->cancel($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->cancel: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\PaymentChargeRequest(); // \Swagger\Client\Model\PaymentChargeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->charge($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->charge: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\ConfirmationRequest(); // \Swagger\Client\Model\ConfirmationRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->onfirm($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->onfirm: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = "order_id_example"; // string | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->ordersOrderId($order_id, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->ordersOrderId: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPaymentRequest(); // \Swagger\Client\Model\NewPaymentRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->pay($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->pay: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPayoutRequest(); // \Swagger\Client\Model\NewPayoutRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->payout($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->payout: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewPaymentRequest(); // \Swagger\Client\Model\NewPaymentRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.

try {
    $result = $apiInstance->publicPay($body, $x_site_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->publicPay: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\RebillRequest(); // \Swagger\Client\Model\RebillRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->rebill($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->rebill: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewRecurringRequest(); // \Swagger\Client\Model\NewRecurringRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurring($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurring: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewRecurringChangeRequest(); // \Swagger\Client\Model\NewRecurringChangeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurringChange($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurringChange: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\RecurringSearchBody(); // \Swagger\Client\Model\RecurringSearchBody | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurringSearch($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurringSearch: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\NewRecurringUnsubscribeRequest(); // \Swagger\Client\Model\NewRecurringUnsubscribeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->recurringUnsubscribe($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->recurringUnsubscribe: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\RefundRequest(); // \Swagger\Client\Model\RefundRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->refund($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->refund: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$transaction_id = "transaction_id_example"; // string | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Сигнатура запроса [description](#section/Request-signature) (при типе авторизации public вместо секретного ключа сервиса используется секретный ключ транзакции)
$x_request_auth = "x_request_auth_example"; // string | Тип авторизации

try {
    $result = $apiInstance->transactionstransactionId($transaction_id, $x_site_id, $x_request_id, $x_request_signature, $x_request_auth);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->transactionstransactionId: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Swagger\Client\Api\OperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\UnsubscribeRequest(); // \Swagger\Client\Model\UnsubscribeRequest | 
$x_site_id = "x_site_id_example"; // string | Идентификатор ТСП
$x_request_id = "x_request_id_example"; // string | Уникальный идентификатор запроса ТСП.
$x_request_signature = "x_request_signature_example"; // string | Request signature. [description](#section/Request-signature)

try {
    $result = $apiInstance->unsubscribe($body, $x_site_id, $x_request_id, $x_request_signature);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationsApi->unsubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

## Documentation for API Endpoints

All URIs are relative to *https://gw.payselection.com/*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*OperationsApi* | [**balance**](docs/Api/OperationsApi.md#balance) | **GET** /balance | 
*OperationsApi* | [**block**](docs/Api/OperationsApi.md#block) | **POST** /payments/requests/block | 
*OperationsApi* | [**cancel**](docs/Api/OperationsApi.md#cancel) | **POST** /payments/cancellation | 
*OperationsApi* | [**charge**](docs/Api/OperationsApi.md#charge) | **POST** /payments/charge | 
*OperationsApi* | [**onfirm**](docs/Api/OperationsApi.md#onfirm) | **POST** /payments/confirmation | 
*OperationsApi* | [**ordersOrderId**](docs/Api/OperationsApi.md#ordersorderid) | **GET** /orders/{OrderId} | 
*OperationsApi* | [**pay**](docs/Api/OperationsApi.md#pay) | **POST** /payments/requests/single | 
*OperationsApi* | [**payout**](docs/Api/OperationsApi.md#payout) | **POST** /payouts | 
*OperationsApi* | [**publicPay**](docs/Api/OperationsApi.md#publicpay) | **POST** /payments/requests/public | 
*OperationsApi* | [**rebill**](docs/Api/OperationsApi.md#rebill) | **POST** /payments/requests/rebill | 
*OperationsApi* | [**recurring**](docs/Api/OperationsApi.md#recurring) | **POST** /payments/recurring | 
*OperationsApi* | [**recurringChange**](docs/Api/OperationsApi.md#recurringchange) | **POST** /payments/recurring/change | 
*OperationsApi* | [**recurringSearch**](docs/Api/OperationsApi.md#recurringsearch) | **POST** /payments/recurring/search | 
*OperationsApi* | [**recurringUnsubscribe**](docs/Api/OperationsApi.md#recurringunsubscribe) | **POST** /payments/recurring/unsubscribe | 
*OperationsApi* | [**refund**](docs/Api/OperationsApi.md#refund) | **POST** /payments/refund | 
*OperationsApi* | [**transactionstransactionId**](docs/Api/OperationsApi.md#transactionstransactionid) | **GET** /transactions/{transactionId} | 
*OperationsApi* | [**unsubscribe**](docs/Api/OperationsApi.md#unsubscribe) | **POST** /payments/unsubscribe | 
*WebhooksApi* | [**webhooks**](docs/Api/WebhooksApi.md#webhooks) | **POST** /webhooks | 
*Webhooks_Api* | [**webhooksRecurring**](docs/Api/Webhooks_Api.md#webhooksrecurring) | **POST** /webhooks_recurring | 
*WebpayApi* | [**create**](docs/Api/WebpayApi.md#create) | **POST** /webpayments/create | 

## Documentation For Models

 - [AccountId](docs/Model/AccountId.md)
 - [AcsUrl](docs/Model/AcsUrl.md)
 - [AddDetails](docs/Model/AddDetails.md)
 - [AddDetailsRecurring](docs/Model/AddDetailsRecurring.md)
 - [AdditionalCheckProps](docs/Model/AdditionalCheckProps.md)
 - [AdditionalUserProps](docs/Model/AdditionalUserProps.md)
 - [Address](docs/Model/Address.md)
 - [Amount](docs/Model/Amount.md)
 - [AnyValue](docs/Model/AnyValue.md)
 - [Balance](docs/Model/Balance.md)
 - [BalanceResponse](docs/Model/BalanceResponse.md)
 - [Bank](docs/Model/Bank.md)
 - [BlockWebhook](docs/Model/BlockWebhook.md)
 - [Brand](docs/Model/Brand.md)
 - [CVC](docs/Model/CVC.md)
 - [CancelResponse](docs/Model/CancelResponse.md)
 - [CancelWebhook](docs/Model/CancelWebhook.md)
 - [Card](docs/Model/Card.md)
 - [CardDetails](docs/Model/CardDetails.md)
 - [CardHolder](docs/Model/CardHolder.md)
 - [CardMasked](docs/Model/CardMasked.md)
 - [CardNumber](docs/Model/CardNumber.md)
 - [Cashier](docs/Model/Cashier.md)
 - [ChangeRecurringByMerchantWebhook](docs/Model/ChangeRecurringByMerchantWebhook.md)
 - [ChangeRecurringStateWebhook](docs/Model/ChangeRecurringStateWebhook.md)
 - [ChargeResponse](docs/Model/ChargeResponse.md)
 - [Client](docs/Model/Client.md)
 - [CoinNameNumber](docs/Model/CoinNameNumber.md)
 - [ConfirmationRequest](docs/Model/ConfirmationRequest.md)
 - [Country](docs/Model/Country.md)
 - [CountryCodeAlpha2](docs/Model/CountryCodeAlpha2.md)
 - [Currency](docs/Model/Currency.md)
 - [CustomFields](docs/Model/CustomFields.md)
 - [CustomerInfo](docs/Model/CustomerInfo.md)
 - [CustomerInfoPayout](docs/Model/CustomerInfoPayout.md)
 - [DateTime](docs/Model/DateTime.md)
 - [DeclineUrl](docs/Model/DeclineUrl.md)
 - [Description](docs/Model/Description.md)
 - [DoublePaymentResponse](docs/Model/DoublePaymentResponse.md)
 - [DoublePaymentResponseAddDetails](docs/Model/DoublePaymentResponseAddDetails.md)
 - [DoubleWebPaymentResponse](docs/Model/DoubleWebPaymentResponse.md)
 - [DoubleWebPaymentResponseAddDetails](docs/Model/DoubleWebPaymentResponseAddDetails.md)
 - [Email](docs/Model/Email.md)
 - [ErrorBalanceResponse](docs/Model/ErrorBalanceResponse.md)
 - [ErrorCode](docs/Model/ErrorCode.md)
 - [ErrorDescription](docs/Model/ErrorDescription.md)
 - [ErrorMessage](docs/Model/ErrorMessage.md)
 - [ErrorResponse](docs/Model/ErrorResponse.md)
 - [ErrorTransactionIdResponse](docs/Model/ErrorTransactionIdResponse.md)
 - [Event](docs/Model/Event.md)
 - [ExpMonth](docs/Model/ExpMonth.md)
 - [ExpYear](docs/Model/ExpYear.md)
 - [ExpirationDate](docs/Model/ExpirationDate.md)
 - [Extra](docs/Model/Extra.md)
 - [ExtraData](docs/Model/ExtraData.md)
 - [ExtraDetails](docs/Model/ExtraDetails.md)
 - [ExtraDetailsCreate](docs/Model/ExtraDetailsCreate.md)
 - [ExtraDetailsPayout](docs/Model/ExtraDetailsPayout.md)
 - [FailWebhook](docs/Model/FailWebhook.md)
 - [FalseCancelResponse](docs/Model/FalseCancelResponse.md)
 - [FalseChargeResponse](docs/Model/FalseChargeResponse.md)
 - [FalsePaymentResponse](docs/Model/FalsePaymentResponse.md)
 - [FalsePayoutResponse](docs/Model/FalsePayoutResponse.md)
 - [FalseRebillResponse](docs/Model/FalseRebillResponse.md)
 - [FalseRecurringResponse](docs/Model/FalseRecurringResponse.md)
 - [FalseRefundResponse](docs/Model/FalseRefundResponse.md)
 - [FalseTokenResponse](docs/Model/FalseTokenResponse.md)
 - [FalseUnsubscribeResponse](docs/Model/FalseUnsubscribeResponse.md)
 - [FederalId](docs/Model/FederalId.md)
 - [Ffd105](docs/Model/Ffd105.md)
 - [Ffd105AgentInfo](docs/Model/Ffd105AgentInfo.md)
 - [Ffd105AgentInfo1](docs/Model/Ffd105AgentInfo1.md)
 - [Ffd105AgentInfoMoneyTransferOperator](docs/Model/Ffd105AgentInfoMoneyTransferOperator.md)
 - [Ffd105AgentInfoPayingAgent](docs/Model/Ffd105AgentInfoPayingAgent.md)
 - [Ffd105AgentInfoReceivePaymentsOperator](docs/Model/Ffd105AgentInfoReceivePaymentsOperator.md)
 - [Ffd105Company](docs/Model/Ffd105Company.md)
 - [Ffd105Items](docs/Model/Ffd105Items.md)
 - [Ffd105SupplierInfo](docs/Model/Ffd105SupplierInfo.md)
 - [Ffd105SupplierInfo1](docs/Model/Ffd105SupplierInfo1.md)
 - [Ffd105Vat](docs/Model/Ffd105Vat.md)
 - [Ffd105Vats](docs/Model/Ffd105Vats.md)
 - [Ffd12](docs/Model/Ffd12.md)
 - [Ffd12Company](docs/Model/Ffd12Company.md)
 - [Ffd12Items](docs/Model/Ffd12Items.md)
 - [Ffd12MarkCode](docs/Model/Ffd12MarkCode.md)
 - [Ffd12MarkQuantity](docs/Model/Ffd12MarkQuantity.md)
 - [Ffd12OperatingCheckProps](docs/Model/Ffd12OperatingCheckProps.md)
 - [Ffd12SectoralItemProps](docs/Model/Ffd12SectoralItemProps.md)
 - [Ffd12SupplierInfo](docs/Model/Ffd12SupplierInfo.md)
 - [Ffd12Vats](docs/Model/Ffd12Vats.md)
 - [GenericResponse](docs/Model/GenericResponse.md)
 - [IP](docs/Model/IP.md)
 - [IPv4](docs/Model/IPv4.md)
 - [IPv6](docs/Model/IPv6.md)
 - [InlineResponse201](docs/Model/InlineResponse201.md)
 - [Interval](docs/Model/Interval.md)
 - [IntervalChange](docs/Model/IntervalChange.md)
 - [IsTest](docs/Model/IsTest.md)
 - [Language](docs/Model/Language.md)
 - [MD](docs/Model/MD.md)
 - [MaxPeriods](docs/Model/MaxPeriods.md)
 - [Model3DSTransactionIdResponse](docs/Model/Model3DSTransactionIdResponse.md)
 - [Model3DSTransactionIdResponseStateDetails](docs/Model/Model3DSTransactionIdResponseStateDetails.md)
 - [Model3DSWebhook](docs/Model/Model3DSWebhook.md)
 - [NewBalanceResponse](docs/Model/NewBalanceResponse.md)
 - [NewCancelResponse](docs/Model/NewCancelResponse.md)
 - [NewChargeResponse](docs/Model/NewChargeResponse.md)
 - [NewPayformRequest](docs/Model/NewPayformRequest.md)
 - [NewPayformSuccessResponse](docs/Model/NewPayformSuccessResponse.md)
 - [NewPaymentRequest](docs/Model/NewPaymentRequest.md)
 - [NewPaymentResponse](docs/Model/NewPaymentResponse.md)
 - [NewPayoutRequest](docs/Model/NewPayoutRequest.md)
 - [NewPayoutResponse](docs/Model/NewPayoutResponse.md)
 - [NewPublicPaymentResponse](docs/Model/NewPublicPaymentResponse.md)
 - [NewRebillResponse](docs/Model/NewRebillResponse.md)
 - [NewRecurringChangeRequest](docs/Model/NewRecurringChangeRequest.md)
 - [NewRecurringChangeResponse](docs/Model/NewRecurringChangeResponse.md)
 - [NewRecurringRequest](docs/Model/NewRecurringRequest.md)
 - [NewRecurringResponse](docs/Model/NewRecurringResponse.md)
 - [NewRecurringSearchAccountRequest](docs/Model/NewRecurringSearchAccountRequest.md)
 - [NewRecurringSearchRebillRequest](docs/Model/NewRecurringSearchRebillRequest.md)
 - [NewRecurringSearchRecurringRequest](docs/Model/NewRecurringSearchRecurringRequest.md)
 - [NewRecurringSearchResponse](docs/Model/NewRecurringSearchResponse.md)
 - [NewRecurringUnsubscribeRequest](docs/Model/NewRecurringUnsubscribeRequest.md)
 - [NewRecurringUnsubscribeResponse](docs/Model/NewRecurringUnsubscribeResponse.md)
 - [NewRefundResponse](docs/Model/NewRefundResponse.md)
 - [NewResponse](docs/Model/NewResponse.md)
 - [NewTokenResponse](docs/Model/NewTokenResponse.md)
 - [NewTransactionIdResponse](docs/Model/NewTransactionIdResponse.md)
 - [NewTransactionIdResponseStateDetails](docs/Model/NewTransactionIdResponseStateDetails.md)
 - [NewUnsubscribeResponse](docs/Model/NewUnsubscribeResponse.md)
 - [NewWebpayRequest](docs/Model/NewWebpayRequest.md)
 - [NewWebpayResponse](docs/Model/NewWebpayResponse.md)
 - [NomenclatureCode](docs/Model/NomenclatureCode.md)
 - [NumberPrice](docs/Model/NumberPrice.md)
 - [NumberThreeFormat](docs/Model/NumberThreeFormat.md)
 - [OneOfFalsePaymentResponseAddDetails](docs/Model/OneOfFalsePaymentResponseAddDetails.md)
 - [OneOfFalseRecurringResponseAddDetails](docs/Model/OneOfFalseRecurringResponseAddDetails.md)
 - [OneOfReceiptDataReceipt](docs/Model/OneOfReceiptDataReceipt.md)
 - [Order](docs/Model/Order.md)
 - [OrderId](docs/Model/OrderId.md)
 - [OrderIdResponse](docs/Model/OrderIdResponse.md)
 - [PaReq](docs/Model/PaReq.md)
 - [PaRes](docs/Model/PaRes.md)
 - [PayToken](docs/Model/PayToken.md)
 - [PaymentCancelRequest](docs/Model/PaymentCancelRequest.md)
 - [PaymentChargeRequest](docs/Model/PaymentChargeRequest.md)
 - [PaymentMethod](docs/Model/PaymentMethod.md)
 - [PaymentToken](docs/Model/PaymentToken.md)
 - [PaymentTokenPaymentDetails](docs/Model/PaymentTokenPaymentDetails.md)
 - [PaymentType](docs/Model/PaymentType.md)
 - [Payments](docs/Model/Payments.md)
 - [PaymentsInner](docs/Model/PaymentsInner.md)
 - [PaymentsSumFormat](docs/Model/PaymentsSumFormat.md)
 - [PaymentsWebhook](docs/Model/PaymentsWebhook.md)
 - [PayoutCard](docs/Model/PayoutCard.md)
 - [PayoutCardPayoutDetails](docs/Model/PayoutCardPayoutDetails.md)
 - [PayoutConfirmationResponse](docs/Model/PayoutConfirmationResponse.md)
 - [PayoutResponse](docs/Model/PayoutResponse.md)
 - [PayoutToken](docs/Model/PayoutToken.md)
 - [PayoutTokenPayoutDetails](docs/Model/PayoutTokenPayoutDetails.md)
 - [PayoutWebhook](docs/Model/PayoutWebhook.md)
 - [PendingTransactionIdResponse](docs/Model/PendingTransactionIdResponse.md)
 - [Period](docs/Model/Period.md)
 - [PeriodChange](docs/Model/PeriodChange.md)
 - [Phone](docs/Model/Phone.md)
 - [PhoneNumber](docs/Model/PhoneNumber.md)
 - [PreauthorizedTransactionIdResponse](docs/Model/PreauthorizedTransactionIdResponse.md)
 - [ProcessingAmount](docs/Model/ProcessingAmount.md)
 - [ProcessingCurrency](docs/Model/ProcessingCurrency.md)
 - [Qiwi](docs/Model/Qiwi.md)
 - [QiwiNumber](docs/Model/QiwiNumber.md)
 - [QiwiPaymentDetails](docs/Model/QiwiPaymentDetails.md)
 - [RRN](docs/Model/RRN.md)
 - [RebillFlag](docs/Model/RebillFlag.md)
 - [RebillId](docs/Model/RebillId.md)
 - [RebillRequest](docs/Model/RebillRequest.md)
 - [RebillResponse](docs/Model/RebillResponse.md)
 - [ReceiptData](docs/Model/ReceiptData.md)
 - [ReceiptEmail](docs/Model/ReceiptEmail.md)
 - [ReceiverNumber](docs/Model/ReceiverNumber.md)
 - [Recurrent](docs/Model/Recurrent.md)
 - [RecurringAmount](docs/Model/RecurringAmount.md)
 - [RecurringData](docs/Model/RecurringData.md)
 - [RecurringId](docs/Model/RecurringId.md)
 - [RecurringNextPay](docs/Model/RecurringNextPay.md)
 - [RecurringPaidCount](docs/Model/RecurringPaidCount.md)
 - [RecurringSearchBody](docs/Model/RecurringSearchBody.md)
 - [RecurringStatus](docs/Model/RecurringStatus.md)
 - [Redirect3DSWebhook](docs/Model/Redirect3DSWebhook.md)
 - [RedirectMethod](docs/Model/RedirectMethod.md)
 - [RedirectParams](docs/Model/RedirectParams.md)
 - [RedirectTransId](docs/Model/RedirectTransId.md)
 - [RedirectTransactionIdResponse](docs/Model/RedirectTransactionIdResponse.md)
 - [RedirectTransactionIdResponseStateDetails](docs/Model/RedirectTransactionIdResponseStateDetails.md)
 - [RedirectUrl](docs/Model/RedirectUrl.md)
 - [RefundRequest](docs/Model/RefundRequest.md)
 - [RefundResponse](docs/Model/RefundResponse.md)
 - [RefundWebhook](docs/Model/RefundWebhook.md)
 - [RegisterRecurringWebhook](docs/Model/RegisterRecurringWebhook.md)
 - [RemainingAmount](docs/Model/RemainingAmount.md)
 - [ReturnUrl](docs/Model/ReturnUrl.md)
 - [SectoralDate](docs/Model/SectoralDate.md)
 - [SectoralNumber](docs/Model/SectoralNumber.md)
 - [SectoralValue](docs/Model/SectoralValue.md)
 - [ServiceId](docs/Model/ServiceId.md)
 - [StartDate](docs/Model/StartDate.md)
 - [StartDateChange](docs/Model/StartDateChange.md)
 - [Status](docs/Model/Status.md)
 - [SuccessTransactionIdResponse](docs/Model/SuccessTransactionIdResponse.md)
 - [SuccessUrl](docs/Model/SuccessUrl.md)
 - [SumNumberTwoFormat](docs/Model/SumNumberTwoFormat.md)
 - [Token](docs/Model/Token.md)
 - [TokenResponse](docs/Model/TokenResponse.md)
 - [TotalFormat](docs/Model/TotalFormat.md)
 - [Town](docs/Model/Town.md)
 - [Transaction](docs/Model/Transaction.md)
 - [TransactionId](docs/Model/TransactionId.md)
 - [TransactionIdResponse](docs/Model/TransactionIdResponse.md)
 - [TransactionState](docs/Model/TransactionState.md)
 - [TransactionStateDetails](docs/Model/TransactionStateDetails.md)
 - [TransactionStatus](docs/Model/TransactionStatus.md)
 - [Type](docs/Model/Type.md)
 - [TypeFormat](docs/Model/TypeFormat.md)
 - [UnsubscribeRecurringWebhook](docs/Model/UnsubscribeRecurringWebhook.md)
 - [UnsubscribeRequest](docs/Model/UnsubscribeRequest.md)
 - [UnsubscribeResponse](docs/Model/UnsubscribeResponse.md)
 - [VatSumFormat](docs/Model/VatSumFormat.md)
 - [VatsSumFormat](docs/Model/VatsSumFormat.md)
 - [VoidedTransactionIdResponse](docs/Model/VoidedTransactionIdResponse.md)
 - [Webhook](docs/Model/Webhook.md)
 - [WebhookRecurring](docs/Model/WebhookRecurring.md)
 - [WebhookUrl](docs/Model/WebhookUrl.md)
 - [WebpayCustomerInfo](docs/Model/WebpayCustomerInfo.md)
 - [WebpayMetaData](docs/Model/WebpayMetaData.md)
 - [WebpayNewPaymentRequest](docs/Model/WebpayNewPaymentRequest.md)
 - [WebpayPaymentRequest](docs/Model/WebpayPaymentRequest.md)
 - [WebpayPaymentType](docs/Model/WebpayPaymentType.md)
 - [WebpayPreviewForm](docs/Model/WebpayPreviewForm.md)
 - [WebpayTypeLink](docs/Model/WebpayTypeLink.md)
 - [ZIP](docs/Model/ZIP.md)

## Documentation For Authorization

 All endpoints do not require authorization.


## Author

support@payselection.com

